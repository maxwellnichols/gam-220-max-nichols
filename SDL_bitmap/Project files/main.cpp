#include <iostream>
#include <string>
#include "SDL.h" // Allows us to actually use the SDL library
using namespace std;

// Set up the class for the image we will be drawing and manipulating
class Image
{
public:
	// Default constructor
	Image(SDL_Renderer* newRenderer, int newWidth, int newHeight)
	{
		// Set the width, height, and what renderer the image will use
		width = newWidth;
		height = newHeight;
		renderer = newRenderer;
		// Create the texture using the same renderer as well as the same width and height (created texture is returned)
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, width, height);
		// If the created texture returned null...
		if (texture == nullptr)
		{
			cout << "Error creating a texture! (" << SDL_GetError() << ")" << endl;
		}
	}

	// Different constructor to load an image
	Image(SDL_Renderer* newRenderer, const std::string& filename)
	{
		renderer = newRenderer;

		// Create the surface from the file
		// c_str gives us the actual string
		SDL_Surface* surface = SDL_LoadBMP(filename.c_str());
		// If the created surface returned null...
		if (surface == nullptr)
		{
			cout << "Error loading surface! (" << SDL_GetError() << ")" << endl;
		}

		// Find width and height after loading the image
		width = surface->w;
		height = surface->h;

		// Create the texture
		texture = SDL_CreateTextureFromSurface(renderer, surface);
		// If the created texture returned null...
		if (texture == nullptr)
		{
			cout << "Error creating a texture! (" << SDL_GetError() << ")" << endl;
		}
	}

	// Now we cannot use the copy constructor
	Image(const Image& other) = delete;

	// Default destructor
	~Image()
	{
		SDL_DestroyTexture(texture);
	}

	// Now we cannot use copy assignment - this takes care of the rule of threes
	Image& operator=(const Image& other) = delete;

	// Frees memory used by the texture without permanently destroying it
	void Free()
	{
		SDL_DestroyTexture(texture);
		texture = nullptr;
	}

	int GetWidth() const
	{
		return width;
	}
	
	int GetHeight() const
	{
		return height;
	}
	// Set this texture as a target for this renderer
	void SetAsTarget()
	{
		SDL_SetRenderTarget(renderer, texture);
	}
	// Unset this texture as a target for this renderer
	void UnsetAsTarget()
	{
		SDL_SetRenderTarget(renderer, nullptr);
	}

	// Draw the 
	// Warning: Very unoptimized/slow to render individual pixels
	void SetPixel(int x, int y, Uint8 red, Uint8 green, Uint8 blue)
	{
		// Set the color to be used as well as the renderer (RGBA - A is always 255 for this instance)
		SDL_SetRenderDrawColor(renderer, red, green, blue, 0xFF);
		// Actually draw that color at this location
		SDL_RenderDrawPoint(renderer, x, y);
	}

	void DrawToScreen(float x, float y)
	{
		SDL_Rect destinationArea = { x, y, width, height };
		SDL_RenderCopy(renderer, texture, nullptr, &destinationArea);
	}

private:
	int width, height;
	SDL_Renderer* renderer;
	SDL_Texture* texture;
};

class Sprite
{
public:
	float x, y;
	Image image;

	// Sprite constructor - uses the image constructor
	Sprite(SDL_Renderer* renderer, const std::string& filename)
		: image(renderer, filename)
	{
		x = 0.0f;
		y = 0.0f;
	}
	// Pass down responsibility to Image
	void Draw()
	{
		image.DrawToScreen(x, y);
	}
};

// This function will actually draw the image pixel by pixel.
void UpdateImage(Image& image)
{
	// Set the SDL texture as a target for the SDL renderer
	image.SetAsTarget();

	// This is what we use to update the image. Each pixel will get its own color and will be drawn seperately. (This is slow)
	for (int row = 0; row < image.GetHeight(); ++row)
	{
		for (int column = 0; column < image.GetWidth(); ++column)
		{
			// Set up the colors we will be using
			Uint8 red;
			Uint8 green;
			Uint8 blue;

			/*
			// CONFIG 1: Diagonal strips
			// Only enable colored pixels if row = column or if row + column = width
			if (row == column || row + column == image.GetWidth())
			{
				// Each pixel that we're enabling will have a cool gradient, like a broken flood fill in Substance Designer
				red = (float(row) / image.GetHeight()) * 255;
				green = (float(column) / image.GetWidth()) * 255;
				blue = 255;
			}
			// Set all disabled pixels to black
			else
			{
				red = 0;
				green = 0;
				blue = 0;
			}
			*/
			/*
			// CONFIG 2: Vertical stripes
			// Only enable colored pixels if row is divisible by 5
			if (column % 5 == 0)
			{
				red = (float(row) / image.GetHeight()) * 255;
				green = (float(column) / image.GetWidth()) * 255;
				blue = 255;
			}
			// Set all disabled pixels to black
			else
			{
				red = 0;
				green = 0;
				blue = 0;
			}
			*/
			/*
			// CONFIG 3: Checkerboard pattern
			// If both column and row is divisible by 2, enable that pixel. If both column and row are NOT divisible by 2, enable that pixel as well
			if (column % 2 == 0 && row % 2 == 0)
			{
				red = (float(row) / image.GetHeight()) * 255;
				green = (float(column) / image.GetWidth()) * 255;
				blue = 255;
			}
			else if (column % 2 != 0 && row % 2 != 0)
			{
				red = (float(row) / image.GetHeight()) * 255;
				green = (float(column) / image.GetWidth()) * 255;
				blue = 255;
			}
			// Set all disabled pixels to black
			else
			{
				red = 0;
				green = 0;
				blue = 0;
			}
			*/
			/*
			// CONFIG 4: Crazy stuff... no idea what this is
			// (rows - h)^2 + (columns - k)^2 -> tells us how far from the center a certain point is... we can use that to gauge intensity...?

			red = (float(row) / image.GetHeight()) * 255;
			green = (float(column) / image.GetWidth()) * 255;
			blue = 255;

			red   -= (((row - (image.GetWidth() / 2)) * (row - (image.GetWidth() / 2))) + ((column - (image.GetHeight() / 2)) * ((column - (image.GetHeight() / 2))))) % 255;
			green -= (((row - (image.GetWidth() / 2)) * (row - (image.GetWidth() / 2))) + ((column - (image.GetHeight() / 2)) * ((column - (image.GetHeight() / 2))))) % 255;
			blue  -= (((row - (image.GetWidth() / 2)) * (row - (image.GetWidth() / 2))) + ((column - (image.GetHeight() / 2)) * ((column - (image.GetHeight() / 2))))) % 255;
			*/

			// CONFIG 5: Vingette
			// Calculate the distance from the center
			float distance = sqrtf(((row - (image.GetWidth() / 2)) * ((row - (image.GetWidth() / 2)))) + ((column - (image.GetHeight() / 2)) * ((column - (image.GetHeight() / 2)))));
			// Set the maximum distance so we don't run into crazy overflow errors
			float maxDistance = 1.414f * (image.GetWidth() / 2);

			// Use the same code as before, but we're now multiplying by the distance as well so that it darkens around the border
			distance /= maxDistance;

			red = (float(row) / image.GetHeight()) * 255 * (1 - distance);
			green = (float(column) / image.GetWidth()) * 255 * (1 - distance);
			blue = 255 * (1 - distance);

			// Run this function for each pixel to actually set it to a certain color
			image.SetPixel(column, row, red, green, blue);
		}
	}

	// Set the image that the SDL renderer uses to a null pointer
	image.UnsetAsTarget();
}

// Special version of the update image function that uses a timer to change color smoothly
// NOTE: This does not work well lol
void UpdateImage(Image& image, int timer)
{
	image.SetAsTarget();

	for (int row = 0; row < image.GetHeight(); row++)
	{
		for (int column = 0; column < image.GetWidth(); column++)
		{
			Uint8 red = (float(row) / image.GetHeight()) * timer;
			Uint8 green = (float(column)/ image.GetWidth()) * (255 - timer);
			Uint8 blue = timer % 255;

			image.SetPixel(column, row, red, green, blue);
		}
	}

	image.UnsetAsTarget();
}

// Main function. Doesn't really need to be commented but it's good to keep track.
int main(int argc, char* argv[])
{
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL failed to initialize! (" << SDL_GetError() << ")" << endl;
		return 1;
	}

	// Create the window to display our image with the the following title...
	SDL_Window* window = SDL_CreateWindow("My Image Generator",
		// Make it centered...
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		// And make it these pizel dimensions
		500, 500, SDL_WINDOW_OPENGL);
	
	if (window == nullptr)
	{
		cout << "Error creating a window! (" << SDL_GetError() << ")" << endl;
		return 2;
	}

	// Create the renderer, which will be used to... render the image output
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr)
	{
		cout << "Error creating a renderer! (" << SDL_GetError() << ")" << endl;
		return 3;
	}

	// Create the image, which will use the renderer and be these pixel dimensions. It should match the window size
	// Image image(renderer, 500, 500);
	// Alternatively we can just use a bitmap thanks to our sprite class
	Sprite outskirts(renderer, "outskirts.bmp");

	// Whether we need to update the image on this frame or not                  - not needed for bitmaps !
	// bool imageNeedsUpdate = true;
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);
	// These keep track of the image's position in the window (top left is 0, 0) - not needed for bitmaps !
	// int x = 0, y = 0;


	// Delta time
	float dt = 0.0f;
	// Keep track of start and end of frame
	Uint32 frameStart;
	Uint32 frameEnd;




	// Keep track of any events that occur (key presses, closing the window, etc)
	SDL_Event event;
	// Render loop. This continues until we close the image window
	bool done = false;
	// Optional timer that increments with every loop
	int timer = 0;

	while (!done)
	{
		// When the frame starts, get the time in MS since the program started
		frameStart = SDL_GetTicks();


		// Process input events and keys pressed this frame
		while (SDL_PollEvent(&event))
		{
			// Check if we closed the window manually
			if (event.type == SDL_QUIT)
			{
				done = true;
			}

			// Check if we pressed Escape (which also closes the window)
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
			}
		}

		// Check for keys held down
		// This will move the image around in the window depending on which arrow key you have held down
		// For more complex games - needs framerate-independent Euler integration

		// How fast the image can move
		float speed = 120.0f;

		if (keystates[SDL_SCANCODE_UP])
		{
			// y--;
			// outskirts.y--;
			// Euler integration:
			outskirts.y -= speed * dt;
		}
		else if (keystates[SDL_SCANCODE_DOWN])
		{
			// y++;
			// outskirts.y++;
			outskirts.y += speed * dt;
		}
		if (keystates[SDL_SCANCODE_LEFT])
		{
			// x--;
			// outskirts.x--;
			outskirts.x -= speed * dt;
		}
		if (keystates[SDL_SCANCODE_RIGHT])
		{
			// x++;
			// outskirts.x++;
			outskirts.x += speed * dt;
		}

		// Similar to in Unity: transform.position += velocity * Time.deltaTime
		// Euler integration kind of sucks but it is fast

		// Set the default background color (RGBA)
		SDL_SetRenderDrawColor(renderer, 100, 0, 0, 255);
		SDL_RenderClear(renderer);

		// Check if we need to update the image                                     - not needed for bitmaps !
		/*
		if (imageNeedsUpdate)
		{
			// For animated images:
			// UpdateImage(image, timer);

			// For static images use the following instead:
			UpdateImage(image);
			imageNeedsUpdate = false;
		}
		*/

		// Draw the image in the window
		// image.DrawToScreen(x, y);
		outskirts.Draw();

		SDL_RenderPresent(renderer);

		// DEBUG: Write position of image to console every frame
		// cout << x << ", " << y << endl;

		// Give some time back to the OS
		SDL_Delay(1);

		/*
		// Increment timer
		timer++;
		cout << timer << endl;

		if (timer >= 255)
		{
			timer = 0;
		}
		*/

		// When the frame ends, get the amount of time that has passed
		frameEnd = SDL_GetTicks();
		// Take the difference - this is how many seconds per frame
		dt = (frameEnd - frameStart) / 1000.0f;
	}

	// Destroy the texture and set the memory it references to a null pointer
	// image.Free();
	outskirts.image.Free();

	// We are done with the program so destroy everything that was using up memory and officially quit the program
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}