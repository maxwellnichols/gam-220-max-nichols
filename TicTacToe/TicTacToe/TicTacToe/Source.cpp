#include <iostream>
#include <string>
#include "Board.h"
#include "Spot.h"
#include "Player.h"
using namespace std;

int main(int argc, char* argv[])
{
	// Set up the board
	Board gameBoard;

	// Set up the players
	Player xPlayer(1);
	Player oPlayer(2);

	// Whose turn it is. 1 for X Player, anything else for O Player
	int playerNum = 1;

	bool win = false;
	bool draw = false;

	while (true)
	{
		// Trick to "clear" the console on Windows 10 or later
		cout << "\033c" << flush;

		// Draw the board
		gameBoard.DrawBoard();

		while (true)
		{
			// Request player input
			cout << "Player " << playerNum << ", please choose a spot." << endl;
			string input;
			getline(cin, input);

			// Pass the player's input into the marked spot
			Spot markedSpot(input);

			// If input is not valid restart the loop
			if (!markedSpot.CheckIfValid())
			{
				cout << "That is not a valid input." << endl;
				// Continue exits the if statement, and restarts the while loop
				continue;
			}

			// Check if the spot is taken
			markedSpot.spot = input[0] - '1';
			if (markedSpot.CheckIfTaken(gameBoard))
			{
				cout << "That spot is already taken." << endl;
				continue;
			}

			// Got a valid spot, update player marker for next turn and leave the loop
			// If it is X Player's turn
			if (playerNum == 1)
			{
				gameBoard.board[markedSpot.spot] = xPlayer.marker;
			}
			// If not it must be O Player's turn
			else
			{
				gameBoard.board[markedSpot.spot] = oPlayer.marker;
			}

			break;
		}

		// Check if somebody won
		win = gameBoard.CheckWin();

		if (gameBoard.CheckWin())
		{
			break;
		}

		// Nobody won. Check if the game is a draw
		draw = gameBoard.CheckDraw();

		if (draw)
		{
			break;
		}

		// Next player's turn
		playerNum++;
		if (playerNum > 2)
			playerNum = 1;
	}

	// Clear the screen
	cout << "\033c" << flush;

	// Draw the board
	gameBoard.DrawBoard();

	if (win)
	{
		cout << "*** Player " << playerNum << " Wins! ***" << endl;
	}
	if (draw)
	{
		cout << "*** It is a draw... ***" << endl;
	}

	cin.get();
	return 0;
}


