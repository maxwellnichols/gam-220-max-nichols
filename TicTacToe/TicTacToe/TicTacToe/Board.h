#pragma once
#include <iostream>
using namespace std;

class Board
{
public:
	Board()
		// We have to declare the values of the board variable like this
		:board{ '1', '2', '3',
			    '4', '5', '6',
			    '7', '8', '9' }
	{

	}

	char board[9];
	bool win = false;

	// Print board to console
	void DrawBoard()
	{
		cout << "-------------" << endl;
		for (int row = 0; row < 3; ++row)
		{
			cout << "| " << board[3 * row] << " | " << board[3 * row + 1] << " | " << board[3 * row + 2] << " |" << endl;
			cout << "-------------" << endl;
		}
		cout << endl;

		return;
	}
	
	// Check for win. Return true if someone won, and false if no one has yet.
	bool CheckWin()
	{
		// Check horizontal win
		for (int row = 0; row < 3; ++row)
		{
			if (board[3 * row] == board[3 * row + 1] && board[3 * row] == board[3 * row + 2])
			{
				return true;
			}
		}

		// Check vertical win
		for (int column = 0; column < 3; ++column)
		{
			if (board[column] == board[3 + column] && board[column] == board[6 + column])
			{
				return true;
			}
		}

		// Check diagonal win
		if (board[0] == board[4] && board[0] == board[8])
		{
			return true;
		}
		if (board[2] == board[4] && board[2] == board[6])
		{
			return true;
		}

		// No one won yet...
		return false;
	}

	// Check for draw. Return true if it's a draw and false if it is not
	bool CheckDraw()
	{
		for (int i = 0; i < 9; ++i)
		{
			// If there is any space on the board that is not an X or an O, the game is not a draw.
			if (board[i] != 'X' && board[i] != 'O')
			{
				return false;
			}
		}

		// If there are no spaces left, the game is a draw.
		return true;
	}
};