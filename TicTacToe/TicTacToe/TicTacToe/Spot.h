#pragma once
#include <iostream>
#include <string>
#include "Board.h"

using namespace std;

// TODO what the hell do I even do here
class Spot
{
public:
	Spot(string pInput)
	{
		input = pInput;
	}

	bool CheckIfValid()
	{
		// Reject empty string or anything outside of 1-9
		if (input.length() == 0 || input[0] < '1' || input[0] > '9')
		{
			// Continue exits the if statement, and restarts the while loop
			return false;
		}
		else
		{
			return true;
		}
	}

	bool CheckIfTaken(Board& gameBoard)
	{
		// If the spot is taken, return true
		if (gameBoard.board[spot] == 'X' || gameBoard.board[spot] == 'O')
		{
			return true;
		}
		// Else it is not taken so continue
		else
		{
			return false;
		}
	}

	string input;
	int spot;
};