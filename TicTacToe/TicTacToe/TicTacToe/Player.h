#pragma once

class Player
{
public:
	Player(int num)
	{
		playerNum = num;

		// Set up the player's marker based on what was passed in during the creation of this object
		if (playerNum == 1)
		{
			marker = 'X';
		}
		else
		{
			marker = 'O';
		}
	}

	// We need to know the player number and the player's marker.
	int playerNum;
	char marker;
};