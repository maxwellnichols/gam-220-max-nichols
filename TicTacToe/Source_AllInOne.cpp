#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	char board[] = { '1', '2', '3',
					 '4', '5', '6',
					 '7', '8', '9' };

	int playerNum = 1;

	bool win = false;
	bool draw = false;

	while (true)
	{
		// Pick the marker for this player
		char marker;
		if (playerNum == 1)
		{
			marker = 'X';
		}
		else
		{
			marker = 'O';
		}

		// Trick to "clear" the console on Windows 10 or later
		cout << "\033c" << flush;

		// Draw the board
		cout << "-------------" << endl;
		for (int row = 0; row < 3; ++row)
		{
			cout << "| " << board[3 * row] << " | " << board[3 * row + 1] << " | " << board[3 * row + 2] << " |" << endl;
			cout << "-------------" << endl;
		}

		// The player chooses a spot
		int spot;
		while (true)
		{
			// Request player input
			cout << "Player " << playerNum << ", please choose a spot." << endl;
			string input;
			getline(cin, input);

			// Reject empty string or anything outside of 1-9
			if (input.length() == 0 || input[0] < '1' || input[0] > '9')
			{
				cout << "That is not a valid input." << endl;
				// Continue exits the if statement, and restarts the while loop
				continue;
			}

			// Reject if the spot is already taken
			spot = input[0] - '1';
			if (board[spot] == 'X' || board[spot] == 'O')
			{
				cout << "That spot is already taken." << endl;
				continue;
			}

			// Got a valid spot, leave the loop
			break;
		}

		board[spot] = marker;

		// Check horizontal win
		win = false;
		for (int row = 0; row < 3; ++row)
		{
			if (board[3 * row] == board[3 * row + 1] && board[3 * row] == board[3 * row + 2])
			{
				win = true;
				break;
			}
		}

		//Check vertical win
		for (int column = 0; column < 3; ++column)
		{
			if (board[column] == board[3 + column] && board[column] == board[6 + column])
			{
				win = true;
				break;
			}
		}

		// Check diagonal win

		if (board[0] == board[4] && board[0] == board[8])
		{
			win = true;
		}
		if (board[2] == board[4] && board[2] == board[6])
		{
			win = true;
		}

		if (win)
		{
			break;
		}

		draw = true;
		for (int i = 0; i < 9; ++i)
		{
			if (board[i] != 'X' && board[i] != 'O')
				draw = false;
		}

		if (draw)
		{
			break;
		}

		// Next player's turn
		playerNum++;
		if (playerNum > 2)
			playerNum = 1;
	}

	// Clear the screen
	cout << "\033c" << flush;

	// Draw the board
	cout << "-------------" << endl;
	for (int row = 0; row < 3; ++row)
	{
		cout << "| " << board[3 * row] << " | " << board[3 * row + 1] << " | " << board[3 * row + 2] << " |" << endl;
		cout << "-------------" << endl;
	}
	cout << endl;

	if (win)
	{
		cout << "*** Player " << playerNum << " Wins! ***" << endl;
	}
	if (draw)
	{
		cout << "*** It is a draw... ***" << endl;
	}

	cin.get();
	return 0;
}


