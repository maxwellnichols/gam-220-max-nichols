#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	// Pointer to store the address before we apply pointer arithmetic to it
	int* beforeAddress;

	// Increment
	cout << "INCREMENT" << endl << "This operation will move the pointer foward in memory to whatever comes after the pointer's current object." << endl;
	// Creating an array with 5 elements in memory
	int* incrementArray = new int[5];
	// Establish the values of the array
	for (int i = 0; i < 5; i++)
	{
		// Add a multiplier to make array values distinct from differences with memory addresses
		incrementArray[i] = i * 4;
	}
	// Create a different pointer to access the same spot in memory. We will do pointer arithmetic on this so that we leave the original in the right place to delete later
	int* incrementPointer = incrementArray;

	// Output the memory address of the incrementPointer variable before we increment it and after we increment it
	cout << "Address before incrementing: " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;
	// Save the address for comparison
	beforeAddress = incrementPointer;
	incrementPointer++;
	cout << "Address after incrementing:  " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;
	// Save the address for comparison again

	// Print the size of an int
	cout << "Size of int: " << sizeof(int) << endl;

	// Find the difference between the two memory addresses and print it... appears to be subtracting values at indexes.
	cout << "Difference between memory addresses: " << incrementPointer << " - " << beforeAddress << " = " << incrementPointer - beforeAddress << endl << endl;


	// Decrement
	cout << "DECREMENT" << endl << "This operation will move the pointer backward in memory to whatever comes before the pointer's current object." << endl;
	// We are still going to use the previous array and pointer for this
	cout << "Address before decrementing: " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;
	// Save the address for comparison
	beforeAddress = incrementPointer;
	incrementPointer--;
	cout << "Address after decrementing:  " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;
	// Save the address for comparison again

	// Print the size of an int
	cout << "Size of int: " << sizeof(int) << endl;

	// Find the difference between the two memory addresses and print it
	cout << "Difference between memory addresses: " << incrementPointer << " - " << beforeAddress << " = " << incrementPointer - beforeAddress << endl << endl;


	// Compound Assignment
	cout << "COMPOUND ASSIGNMENT" << endl << "This operation will move the pointer around in memory depending on what is entered." << endl;
	// Set up variables for user input
	int userNum;
	string userInput;
	// Request integer. This is what we will move around by
	cout << "Enter an integer to change where the pointer looks at (0-4 is safe): ";
	getline(cin, userInput);
	userNum = stoi(userInput);
	cout << endl;

	cout << "Address before compound assignment: " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;
	beforeAddress = incrementPointer;
	// CHange it based on what the user input
	incrementPointer += userNum;
	cout << "Address after compound assignment:  " << incrementPointer << " (Value: " << *incrementPointer << ")" << endl;

	// Print size of int...
	cout << "Size of int: " << sizeof(int) << endl;

	// Find the difference between the two memory addresses and print it
	cout << "Difference between memory addresses: " << incrementPointer << " - " << beforeAddress << " = " << incrementPointer - beforeAddress << endl << endl;


	// We are done with these pointers, so free the memory
	delete[] incrementArray;

	cin.get();
	return 0;
}