#pragma once
#include <iostream>
#include "Monster.h"
using namespace std;

class Boss : public Monster
{
public:
	Boss()
	{
		// The boss monster has extra health but still does 1 damage
		name = "Dungeon Keeper";
		encounter = "The Dungeon Keeper slowly steps forwards.";
		health = 3;
		attack = 1;
	}

	virtual string GetFlavorText() override
	{
		// Init random seed
		srand(time(NULL));
		// Randomly generate a number (1, 2, or 3)
		int random = rand() % 3 + 1;

		// Output flavor text
		if (random == 1)
		{
			return "The Dungeon Keeper laughs menacingly.";
		}
		else if (random == 2)
		{
			return "The Dungeon Keeper showers itself with gold coins.";
		}
		else
		{
			return "The Dungeon Keeper twirls its sword in a fancy manner.";
		}
	}

	virtual string GetDamageText() override
	{
		return "The Dungeon Keeper slices at you with its golden sword!";
	}
};