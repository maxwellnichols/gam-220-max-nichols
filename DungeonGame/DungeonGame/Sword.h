#pragma once
#include <string>
#include "Item.h"
using namespace std;

class Sword : public Item
{
public:
	// Attack strength is unique to swords
	int attack;

	Sword()
	{
		name = "Iron Sword";
		description = "Not a fantastic weapon, but it's all you have right now.";
		// Attack power of sword
		attack = 1;
	}

	// Get attack strength
	int GetAttackPower()
	{
		return attack;
	}
};