#pragma once
#include <string>
#include "Item.h"
using namespace std;

class Shield : public Item
{
public:
	// Shields do not do anything special, but if we have one in our inventory we can block damage
	Shield()
	{
		name = "Wooden Shield";
		description = "Looks pretty beat up, but it can probably still block attacks.";
	}
};