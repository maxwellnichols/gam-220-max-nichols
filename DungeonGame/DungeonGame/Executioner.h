#pragma once
#include <iostream>
#include "Monster.h"
using namespace std;

class Executioner : public Monster
{
public:
	Executioner()
	{
		// Executioners do extra damage but only have a little health
		name = "Executioner";
		encounter = "An executioner emerges from the shadows. Better be careful - its axe looks painful.";
		health = 1;
		attack = 2;
	}

	virtual string GetFlavorText() override
	{
		// Init random seed
		srand(time(NULL));
		// Randomly generate a number (1, 2, or 3)
		int random = rand() % 3 + 1;

		// Output flavor text
		if (random == 1)
		{
			return "The executioner adjusts its mask.";
		}
		else if (random == 2)
		{
			return "The executioner sharpens its axe.";
		}
		else
		{
			return "The executioner seems like it's having a bad day.";
		}
	}

	virtual string GetDamageText() override
	{
		return "The executioner chops you with its heavy axe!";
	}
};