#pragma once
#include <iostream>
#include "Monster.h"
using namespace std;

class Skeleton : public Monster
{
public:
	Skeleton()
	{
		// Skeletons are basic and have 1 HP and 1 attack
		name = "Skeleton";
		encounter = "A skeleton popped out!";
		health = 1;
		attack = 1;
	}
	
	virtual string GetFlavorText() override
	{
		// Init random seed
		srand(time(NULL));
		// Randomly generate a number (1, 2, or 3)
		int random = rand() % 3 + 1;

		// Output flavor text
		if (random == 1)
		{
			return "The skeleton rattles its bones at you.";
		}
		else if (random == 2)
		{
			return "The skeleton looks like it has a bone to pick with you.";
		}
		else
		{
			return "You feel like this skeleton is going to give you a bad time.";
		}
	}
	
	virtual string GetDamageText() override
	{
		return "The skeleton rattles around and takes a swipe at you!";
	}
};