#pragma once
#include <iostream>
#include "Monster.h"
using namespace std;

class BuffSkeleton : public Monster
{
public:
	BuffSkeleton()
	{
		// Buff skeletons have a little more HP but don't hit as hard as you might think
		name = "Buff Skeleton";
		encounter = "A buff skeleton flexes its muscles at you.";
		health = 2;
		attack = 1;
	}

	virtual string GetFlavorText() override
	{
		// Init random seed
		srand(time(NULL));
		// Randomly generate a number (1, 2, or 3)
		int random = rand() % 3 + 1;

		// Output flavor text
		if (random == 1)
		{
			return "The buff skeleton flexes its muscles at you again.";
		}
		else if (random == 2)
		{
			return "The buff skeleton drinks a protein shake.";
		}
		else
		{
			return "The buff skeleton jogs in place.";
		}
	}

	virtual string GetDamageText() override
	{
		return "The buff skeleton socks you in the face!";
	}
};