#pragma once
#include <iostream>
#include <stdlib.h>  // for the enemy RNG
#include <time.h>    // for random seed
#include <string>
using namespace std;

class Monster
{
public:
	// Name
	string name;
	// Encounter description
	string encounter;
	// Health
	int health = 1;
	// Attack power
	int attack = 1;

	// Allow it to take damage
	void TakeDamage(int strength)
	{
		health -= strength;
	}

	// Each monster will have a few lines of flavor text. Calling this function will pick one at random.
	virtual string GetFlavorText() = 0;
	// Each monster will also have unique text when they damage you
	virtual string GetDamageText() = 0;
};