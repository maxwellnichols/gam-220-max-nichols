#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Monster.h"
#include "Item.h"
using namespace std;

class Room
{
public:
	// Things that each room needs to keep track of
	// Room's name
	string name;
	// Room's description
	string description;
	// List of monsters
	vector<Monster*> monsterList;
	// What rooms connect to it
	vector<Room*> connectedRooms;
	// What items the room has
	vector<Item*> roomItems;

	// Whenever we make a room, we need to know its name and its description
	Room(string rName, string rDescription)
	{
		name = rName;
		description = rDescription;
	}
	// Add a room to the list of connected rooms
	void AddConnection(Room* roomToConnect)
	{
		connectedRooms.push_back(roomToConnect);
	}
	// Add a monster to the room - it can be Skeleton or Boss so we need to use a template
	template<typename T>
	void AddMonster(T* monsterToAdd)
	{
		monsterList.push_back(monsterToAdd);
	}
	// If this is not 0, we fight a monster
	int GetNumberOfMonsters()
	{
		return monsterList.size();
	}
	// Useful for iterating through rooms
	int GetNumberOfConnectedRooms()
	{
		return connectedRooms.size();
	}
	// Give the description back
	string GetDescription()
	{
		return description;
	}

	// Add an item to the room
	void AddItem(Item* itemToAdd)
	{
		roomItems.push_back(itemToAdd);
	}
	// See how many items the room has
	int GetNumberOfItems()
	{
		return roomItems.size();
	}
};