#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;

class Player
{
public:
	Player()
	{
		health = 3;
		hasShield = false;
		canPickUpShield = false;
	}

	void TakeDamage(int strength)
	{
		health -= strength;
	}

	void Heal(int amount)
	{
		health += amount;

		// Cap health at 3 HP
		if (health > 3)
		{
			health = 3;
		}
	}

	// Called when we first find the shield
	void GetShield()
	{
		hasShield = true;
		canPickUpShield = true;
	}
	
	// How much health the player has - if 0, game over!
	int health;
	// Whether the player has the shield or not
	bool hasShield;
	// Whether the player can pick the shield up after combat or not
	bool canPickUpShield;

	// Keep track of the player's inventory
	vector<Item*> inventory;
	// Inventory operations...
	// Add an item to the inventory
	template<typename T>
	void AddItem(T* itemToAdd)
	{
		inventory.push_back(itemToAdd);
	}
	// Get size of inventory for displaying it with check
	int GetNumberOfInvItems()
	{
		return inventory.size();
	}
};