#pragma once
#include <iostream>
#include <string>
#include <stdlib.h>  // for the enemy RNG
#include <time.h>    // for random seed
#include <stdexcept>
#include <typeinfo>
#include "Room.h"
#include "Player.h"
#include "Skeleton.h"
#include "BuffSkeleton.h"
#include "Executioner.h"
#include "Boss.h"
#include "Sword.h"
#include "Shield.h"
using namespace std;

class Game
{
public:
	// Keep track of our player
	Player player;

	void Run()
	{
		// Set up the rooms...
		// Maybe do this from a file eventually, but this is OK for now
		Room* entryway      = new Room("Main Entryway",          "This room almost makes you forget that you're in a dungeon.\nIt looks quite pleasant - but that might be due to the small beams of sunlight peaking down the steps.");
		Room* centralHub    = new Room("Central Hub",            "It starts to get a lot darker in this room. There are many ways to go -\nyou remember being told to check the storage chamber for equipment left by failed adventurers.");
		Room* storage       = new Room("Storage Chamber",        "Boxes, crates, and barrels fill the room, placed haphazardly everywhere.\nBroken swords and busted armor line the walls, some resting on the corpses of past adventurers.");
		Room* crossroads    = new Room("Crossroads",             "This room branches off in multiple directions.\nIt looks pretty well-kept for a room about halfway through a dungeon.");
		Room* gallows       = new Room("Gallows",                "This room seems to have just about every torture device ever invented. Curiously, they're missing JavaScript.\nSome of the devices are occupied by skeletons or rotting corpses.");
		Room* holdingCells  = new Room("Holding Cells",          "You look around and see loads of skeletons trapped in battered and broken cages.\nIn the corner of your eye, some of them look like they're moving around...");
		Room* treasureEntry = new Room("Treasure Room Entryway", "You see glimmers of gold in the room just ahead.\nCould you be getting close to the treasure...? This almost seems too easy...");
		Room* treasureRoom  = new Room("Treasure Chamber",       "You see mountains of gold in all directions, with gems and diamonds lining the room.\nThis seems like the perfect place to start a money swimming pool.");

		// Init room connections
		entryway->AddConnection(centralHub);

		centralHub->AddConnection(storage);
		centralHub->AddConnection(crossroads);
		centralHub->AddConnection(entryway);

		storage->AddConnection(centralHub);

		crossroads->AddConnection(holdingCells);
		crossroads->AddConnection(gallows);
		crossroads->AddConnection(centralHub);

		gallows->AddConnection(treasureEntry);
		gallows->AddConnection(crossroads);

		holdingCells->AddConnection(treasureEntry);
		holdingCells->AddConnection(crossroads);

		treasureEntry->AddConnection(treasureRoom);
		treasureEntry->AddConnection(holdingCells);
		treasureEntry->AddConnection(gallows);

		// Init the monsters
		Skeleton	 room2Skeleton;
		BuffSkeleton room3Skeleton;
		Executioner	 room5Skeleton;
		Skeleton	 room6Skeleton;
		BuffSkeleton room7Skeleton;
		Boss		 finalBoss;
		// We are adding these to a list of Monsters for each room
		centralHub->AddMonster(&room2Skeleton);
		storage->AddMonster(&room3Skeleton);
		gallows->AddMonster(&room5Skeleton);
		holdingCells->AddMonster(&room6Skeleton);
		treasureEntry->AddMonster(&room7Skeleton);
		treasureRoom->AddMonster(&finalBoss);

		// Give the player a sword to start. We could do this anywhere but it's ok to do it here
		Sword ironSword;
		player.AddItem(&ironSword);

		// Add a shield to the storage room
		Shield woodenShield;
		storage->AddItem(&woodenShield);

		// Whether to stop the loop or not
		bool done = false;
		// Whether we won or not
		bool win = false;

		// Pointer to current room
		Room* currentRoom = entryway;

		// Explain the game to the player
		cout << "You stand at the entrance to a grimy, old, dungeon.\nFor ages, there have been rumors circulating of untold riches - and danger - deep within." << endl;
		cout << "Many have attempted to conquer the dungeon before, but no one has ever come back alive.\nWill you be the one to finally do it?" << endl;
		cout << "As you decend the steps, the warm sunshine outside slowly fades away." << endl << endl;
		PressEnter();

		// Main game loop...
		while (!done)
		{
			// Clear screen
			Clear();

			// If that room has a monster, start combat loop
			if (currentRoom->GetNumberOfMonsters() > 0)
			{
				// Pass in the current room so that we can find the monster we need to fight
				DoCombatLoop(currentRoom);

				// hacky but check if the treasure room has 0 enemies - if yes, the boss is dead and we win
				if (treasureRoom->GetNumberOfMonsters() == 0)
				{
					done = true;
					win = true;
				}
			}

			// If the player is still alive, the game continues
			if (player.health > 0 && !win)
			{
				// Display information about current room, as well as valid inputs
				cout << "You stand in the " << currentRoom->name << "." << endl;
				cout << "What will you do?" << endl << "(actions: look, move, check)" << endl;

				// Ask for user input - move (change rooms), look (get description), check (get player HP and shield status)
				string input = GetInput();

				// Player is looking around
				if (input == "look")
				{
					Clear();
					// Get room description
					cout << currentRoom->GetDescription() << endl;

					// Check if the room has any items
					if (currentRoom->GetNumberOfItems() > 0)
					{
						// Go through the list of items and pick them all up
						for (int i = 0; i < currentRoom->GetNumberOfItems(); i++)
						{
							// Add the item to the inventory...
							player.AddItem(currentRoom->roomItems[i]);
							// Output text...
							cout << endl << "You spot a " << currentRoom->roomItems[i]->GetItemName() << " on the ground. " << currentRoom->roomItems[i]->GetItemDesc() << "\nYou decide to take it with you." << endl;
						}

						// PLACEHOLDER - Very dumb and hacky thing to make sure the game is completable... if we picked up an item pretend it was a shield to work with the hardcoded shield code
						// DEFINITELY UPDATE IF POSSIBLE - this just makes sure the game is finishable
						player.GetShield();

						// Clear the room item vector so we never double-pick anything up
						currentRoom->roomItems.clear();
					}

					PressEnter();
				}
				// Player wants to move.
				else if (input == "move")
				{
					cout << endl << "Where do you want to go?" << endl;
					// Print out available rooms
					for (int i = 0; i < currentRoom->GetNumberOfConnectedRooms(); i++)
					{
						cout << i + 1 << ") " << currentRoom->connectedRooms.at(i)->name << endl;
					}
					// Get user input
					input = GetInput();

					// The player can easily mess this up - catch it if it's not valid and get them to try again
					try
					{
						// Make sure it's valid
						if ((stoi(input) - 1) >= 0 && (stoi(input) - 1) <= currentRoom->GetNumberOfConnectedRooms())
						{
							// Change the current room
							currentRoom = currentRoom->connectedRooms.at(stoi(input) - 1);
						}
						// Invalid location
						else
						{
							cout << "That is not a valid option." << endl;
							PressEnter();
						}
					}
					catch (exception& e)
					{
						cout << "Invalid location." << endl;
						PressEnter();
					}
				}
				else if (input == "check")
				{
					/*
					// Get player health and shield status
					Clear();
					cout << "You are standing in the " << currentRoom->name << "." << endl;
					cout << "You have " << player.health << "/3 HP. You wield an iron sword. It's not great, but it will do." << endl;

					if (player.hasShield)
					{
						cout << "You also wield a battered-up shield. It seems to fly out of your hands quite easily..." << endl;
					}
					*/

					// Get player location and health
					Clear();
					cout << "You are standing in the " << currentRoom->name << "." << endl;
					cout << "You have " << player.health << "/3 HP." << endl << endl << "Inventory:" << endl;
					// Run through the inventory and output each item's name and description
					for (int i = 0; i < player.GetNumberOfInvItems(); i++)
					{
						cout << player.inventory[i]->name << " - " << player.inventory[i]->description << endl;
					}
					cout << endl;
					PressEnter();
				}
				else
				{
					cout << endl << "That is not a valid input." << endl;
					PressEnter();
				}
			}
			// If the player is dead exit the loop
			else
			{
				done = true;
			}
		}

		// Game is done. If the player beat the final boss, show win text. If they died... RIP I guess
		if (win)
		{
			// Win stuff
			cout << "With the Dungoen Keeper slain, the riches are all yours." << endl;
			cout << "The ground shakes - the dungeon is beginning to crumble! You drop your equipment and grab an armful of gold." << endl << endl;
			// TODO - maybe we can do an escape scene where the room descriptions change and the win condition is getting to the entryway instead?
			cout << "You narrowly escape the collapsing dungeon. As you bolt up the stairs, a large stone falls behind you,\ntrapping the rest of the riches inside." << endl;
			cout << endl << "YOU WIN!" << endl << endl;
			PressEnter();
		}
	}

	void DoCombatLoop(Room* battleRoom)
	{
		// Keep track of the round result. I wanted to use a boolean, but you can't really use a boolean when there's 3 possible cases
		enum class RoundResult
		{
			PLAYERWIN,
			PLAYERLOSE,
			TIE
		};
		// Default to a lose state
		RoundResult roundResult = RoundResult::PLAYERLOSE;

		// Clear screen
		Clear();
		// We will always work with the first monster in the list. Even though rooms will never have more than one monster, this theoretically allows us to do so
		cout << battleRoom->monsterList[0]->encounter << endl;
		cout << "Looks like it wants to play Rock Paper Scissors..." << endl << endl;

		// This becomes true if the player dies
		bool battleDone = false;

		while (!battleDone)
		{
			// Player chooses Rock Paper or Scissors
			cout << "What will you play?" << endl;
			cout << "1) Rock" << endl << "2) Paper" << endl << "3) Scissors" << endl;
			// Get input
			string input = GetInput();

			// TODO Weird things happen if you don't input something valid, not sure what to do about it, but it doesn't crash anything... ----------------------

			// Monster chooses Rock Paper or Scissors
			string monsterInput = DoMonsterRng();

			// We can probably do this better with an enum, but I don't feel like it right now
			if (monsterInput == "1")
			{
				cout << endl << "The " << battleRoom->monsterList[0]->name << " plays Rock." << endl;
			}
			else if (monsterInput == "2")
			{
				cout << endl << "The " << battleRoom->monsterList[0]->name << " plays Paper." << endl;
			}
			else if (monsterInput == "3")
			{
				cout << endl << "The " << battleRoom->monsterList[0]->name << " plays Scissors." << endl;
			}
			cout << endl;

			// By default, the player loses the round - this gets changed if they win
			roundResult = RoundResult::PLAYERLOSE;

			// We only need to keep track of scenarios where the player wins
			if ((input == "1" && monsterInput == "3") || (input == "2" && monsterInput == "1") || (input == "3" && monsterInput == "2"))
			{
				roundResult = RoundResult::PLAYERWIN;
			}
			// ...or if there is a draw
			else if (input == monsterInput)
			{
				roundResult = RoundResult::TIE;
			}

			// If the player lost, the monster gets to attack
			if (roundResult == RoundResult::PLAYERLOSE)
			{
				// If player does not have shield up, the player should take damage.
				if (!player.hasShield)
				{
					// Get attack strength from monster and deal damage
					player.TakeDamage(battleRoom->monsterList[0]->attack);

					// Check if the player is dead
					if (player.health <= 0)
					{
						cout << battleRoom->monsterList[0]->GetDamageText() << " You have been slain..." << endl;
						cout << endl << "GAME OVER!" << endl << endl;
						battleDone = true;
						PressEnter();
					}
					// Player is not dead, but they still took damage
					else
					{
						cout << battleRoom->monsterList[0]->GetDamageText() << " You're down to " << player.health << "/3 HP!" << endl;
					}
				}
				// 
				else
				{
					cout << "You deflect the " << battleRoom->monsterList[0]->name << "'s attack with your shield! The shield drops to the floor." << endl;
					player.hasShield = false;
				}
			}
			// If there was a draw, nothing happens.
			else if (roundResult == RoundResult::TIE)
			{
				cout << "You and the " << battleRoom->monsterList[0]->name << " clash, deflecting each others' attacks." << endl;
				PressEnter();
			}

			// If the player is still alive and they did not tie, they attack regardless of if they won the round or not
			if (!battleDone && roundResult != RoundResult::TIE)
			{
				cout << "You attack the " << battleRoom->monsterList[0]->name << ".";
				battleRoom->monsterList[0]->TakeDamage(1);

				// Check if the monster is dead
				if (battleRoom->monsterList[0]->health <= 0)
				{
					cout << endl << "The " << battleRoom->monsterList[0]->name << " falls to the ground. You are victorious!" << endl;

					// Pick up your shield if you are not holding it and are able to pick it up
					if (!player.hasShield && player.canPickUpShield)
					{
						cout << endl << "You grab your shield off of the floor." << endl;
						player.hasShield = true;
					}
					// Get rid of the monster in the first slot
					battleRoom->monsterList.erase(battleRoom->monsterList.begin());

					// Recover 1 HP if your HP is 1
					if (player.health == 1)
					{
						cout << endl << "You recover some energy." << endl;
						player.Heal(1);
					}

					// Battle is finished
					battleDone = true;
				}
				// Monster is not dead. Communicate this
				else
				{
					cout << " The " << battleRoom->monsterList[0]->name << " staggers backwards, looking a little weaker." << endl;
				}

				PressEnter();
			}

			// No one is dead, clear screen and repeat loop...
			Clear();

			// Get flavor text - careful, if the battle isn't done we will be referencing something that doesn't exist
			if (!battleDone)
			{
				cout << battleRoom->monsterList[0]->GetFlavorText() << endl << endl;
			}
		}

		// End of battle
	}

	void Clear()
	{
		// Really bad way to clear the screen but shhh it's okay
		system("cls");
	}

	string GetInput()
	{
		// Get user input and return it
		string result;

		cout << "> ";
		getline(cin, result);

		return result;
	}

	string DoMonsterRng()
	{
		// Init random seed
		srand(time(NULL));
		// Randomly generate a number (1, 2, or 3)
		int random = rand() % 3 + 1;

		// Convert to a string. This will never fail
		string randomString = to_string(random);
		// Return value
		return randomString;
	}

	void PressEnter()
	{
		cout << "Press Enter to continue..." << endl;
		cin.get();
	}
};