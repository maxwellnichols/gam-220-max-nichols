#pragma once
#include <string>
using namespace std;

class Item
{
public:
	string name;
	string description;

	Item()
	{
		name = "Generic Item";
		description = "You shouldn't be able to get this.";
	}

	string GetItemName()
	{
		return name;
	}
	string GetItemDesc()
	{
		return description;
	}
};