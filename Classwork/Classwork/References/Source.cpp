#include <iostream>
using namespace std;

// Wherever VERSION_NUMBER appears in the code, it will be replaced with 3. No computation time with #define
#define VERSION_NUMBER 3


// Pass by value / Value semantics
// Add5 requires an integer to be passed in. It will return the same integer plus 5.
// This makes a copy of the variable.
// This is the default for C++, unless explicitly stated (below)
int Add5(int number)
{
	number += 5;
	return number;
}

// Pass by reference / Reference semantics
// int& is a reference type. It is not representing a value, it is representing a different variable that exists outside this function.
// This is useful for big variable types (classes, etc)
void Add10(int& number)
{
	number += 10;
}

int main(int argc, char* argv[])
{
	// Give a value to the function. The value returns it after copying and changing it
	int result = Add5(7);

	cout << "Add5: " << result << endl;

	int input = 7;

	// We are making a copy of input and storing it as "result". This should not change the actual original input variable.
	result = Add5(input);
	cout << "Input after Add5: " << input << endl;

	// We are directly changing input. This will change the value of the original input variable.
	Add10(input);
	cout << "Input after Add10: " << input << endl;

	// Add10(8); doesn't work.
	// Can't convert an integer to an integer reference (you would be changing the value of 8)
	// Must be a variable that can appear on the left side of the equation (lvalue), like something that can store information

	// int& steve; doesn't work.
	// Cannot initialize a reference variable - needs to actually reference something

	int& steve = input; 
	steve += 16;
	// Whenever we modify steve, input will be updated, and vice versa.

	cout << "v" << VERSION_NUMBER << endl;

	// Check if OTHER_NUMBER is even defined. If yes, compile it. If no, ignore it.
	// In this case, OTHER_NUMBER is only defined in debug mode (due to project settings).
#ifdef OTHER_NUMBER
	cout << "Other number: " << OTHER_NUMBER << endl;
#endif

	// Works with standard #if too. This is useful for compiling for different consoles or platforms.
#if VERSION_NUMBER == 3
	cout << "Version is 3!" << endl;
#endif
#if VERSION_NUMBER == 5
	cout << "Version is 5!" << endl;
#endif

	cin.get();
	return 0;
}