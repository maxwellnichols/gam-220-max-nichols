#include <iostream>
using namespace std;

// Function to print everything in the array
void PrintArray(int* array, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << array[i] << " ";
	}
	cout << endl;
}

// Function to resize any array, given an array input, an initial size, and a requested size (whether bigger or smaller)
// Pass by reference to change the actual pointer
void ResizeArray(int*& array, int initialSize, int requestedSize)
{
	int* initialArray = array;
	int* resizerArray = new int[requestedSize];

	// Deep copy values from input array to new array. Use the smaller of the two size inputs to allow for growing and shrinking
	if (initialSize < requestedSize)
	{
		for (int i = 0; i < initialSize; i++)
		{
			resizerArray[i] = initialArray[i];
		}
	}
	else if (requestedSize < initialSize)
	{
		for (int i = 0; i < requestedSize; i++)
		{
			resizerArray[i] = initialArray[i];
		}
	}
	else // initial size = requested size
	{
		cout << "Literally why are you even calling this function if you're not even resizing the array? Bruh" << endl;
		return;
	}

	// Delete the original array and make it point to the resizer
	delete[] initialArray;
	array = resizerArray;
}


int main(int argc, char* argv[])
{
	// To declare an array on the stack
	int stackArray[6];
	for (int i = 0; i < 6; i++)
	{
		stackArray[i] = i + 1;
	}
	// Print it
	cout << "Stack Array: ";
	PrintArray(stackArray, 6);

	// To declare an array on the heap
	int* heapArray;
	// Assign a new memory address to heapArray - a sequence of 10 integers
	heapArray = new int[10];
	for (int i = 0; i < 10; i++)
	{
		heapArray[i] = i * 2;
	}
	// Print it
	cout << "Heap Array: ";
	PrintArray(heapArray, 10);

	// Delete memory allocated for heapArray
	delete[] heapArray;
	// We can't use this memory anymore!
	// cout << heapArray[3] << endl;

	// We can mark that this pointer does not point to anything anymore. This is handy because we can check for it
	heapArray = nullptr;
	// If heapArray isn't a null pointer, print it out
	if (heapArray != nullptr)
	{
		PrintArray(heapArray, 10);
	}
	else
	{
		cout << "Pointer is null!" << endl;
	}

	// Set heapArray to point to a new set of integers
	heapArray = new int[4];
	// Copy values from stackArray into heapArray - we can only copy the first four since heapArray only points to four ints
	// This is a deep copy, even though it's not copying every single value
	for (int i = 0; i < 4; i++)
	{
		heapArray[i] = stackArray[i];
	}
	// Print it
	cout << "Heap Array after copy: ";
	PrintArray(heapArray, 4);

	// Resizing the heap array
	// Create a new array to store values in
	int* tempArray = new int[20];
	// Copy values from heapArray to the new array. We can only access the first four or else we get an error (nothing is there)
	for (int i = 0; i < 4; i++)
	{
		tempArray[i] = heapArray[i];
	}
	// Delete the heap array, and then make it point to tempArray instead
	delete[] heapArray;
	heapArray = tempArray;
	// Print it. This has a bunch of garbage data, but we own it, so we can print it
	cout << "Heap Array after resize: ";
	PrintArray(heapArray, 20);

	// Change how cout processes integers - here we change it to hexadecimal, and then back to decimal
	cout << "Heap Array [7] in hexadecimal: " << std::hex << heapArray[7] << std::dec << endl;

	// We are done with heapArray so we need to delete it to free memory
	delete[] heapArray;


	// Test the resizeArray function
	int* testArray = new int[4];
	for (int i = 0; i < 4; i++)
	{
		testArray[i] = i;
	}

	cout << endl;
	cout << "testArray before resize: ";
	PrintArray(testArray, 4);
	
	ResizeArray(testArray, 4, 8);

	cout << "testArray after resize: ";
	PrintArray(testArray, 8);


	cin.get();
	return 0;
}