#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// Set aside space in memory and set the value to 5.
	int bob = 5;

	// A pointer is a memory address
	// This will be a memory address, and there we will store an integer
	int* bobsAddress = &bob; // "Address of" operator. References bob

	// Output bob normally
	cout << "Bob: " << bob << endl;
	// Output the actual memory address (stored in hexadecimal)
	cout << "Bob's Address: " << bobsAddress << endl;
	// Reference a pointer to access what is stored there (Dereferencing)
	cout << "Bob via Memory Address: " << *bobsAddress << endl;

	// Change bob by dereferencing his address
	*bobsAddress = 17;

	// Reassign bob's address to definitelyNotBob instead of bob
	int definitelyNotBob = 10;
	bobsAddress = &definitelyNotBob;

	// Set up a spot in memory that is allocated to this pointer, without declaring what it points to
	int* anotherIntPointer = new int;
	*anotherIntPointer = 23;

	// In C++ we have to manage our own memory
	// Delete variables when we're done using them. Always match a "new" with a "delete." If we don't, we will cause a memory leak!
	delete anotherIntPointer;

	/* DO NOT DO THIS
	while(true)
	{
		int* ptr = new int;
	}
	*/

	cin.get();
	return 0;
}