// Language translation - C# to C++

// using System;
#include <iostream>
using namespace std;

// class Program
// {
	// static void Main(string[] args)
	int main(int argc, char* argv[])
	{
		// Brackets go on array variable instead of array declaration
		// int[] myArray = { 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70 };
		int myArray[] = { 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70 };
		// Declare array length
		int length = 15;

		float sum = 0;
		// C++ does not have array length - they are just a sequence of values
		// This is annoying but we just use smarter types of variables more often

		// for loops work exactly the same between C++ and C#
		// for (int i = 0; i < myArray.Length; ++i)
		for (int i = 0; i < length; ++i)
		{
			sum += myArray[i];
		}
		// C++ has no string addition operator
		// Console.WriteLine("Sum of all numbers: " + sum);
		cout << "Sum of all numbers: " << sum << endl;

		// float avg = sum / myArray.Length;
		float avg = sum / length;

		// Console.WriteLine("Average: " + avg);
		cout << "Average: " << avg << endl;

		// for (int i = 0; i < myArray.Length; ++i)
		for (int i = 0; i < length; ++i)
		{
			if (myArray[i] < 70)
			{
				// Console.WriteLine(myArray[i] + " is less than 70");
				cout << myArray[i] << " is less than 70" << endl;
				break;
			}
		}

		// Console.WriteLine("Printing numbers greater than 100");
		cout << "Printing numbers greater than 100" << endl;

		// for (int i = 0; i < myArray.Length; i++)
		for (int i = 0; i < length; i++)
		{
			if (myArray[i] > 100)
			{
				// Console.Write(myArray[i] + " ");
				cout << myArray[i] << " " << endl;
			}
		}
		// Console.WriteLine();
		cout << endl;

		int maxSoFar = -9999;
		int minSoFar = 9999;
		// for (int i = 0; i < myArray.Length; i++)
		for (int i = 0; i < length; i++)
		{
			if (myArray[i] > maxSoFar)
			{
				maxSoFar = myArray[i];
			}
			else if (myArray[i] < minSoFar)
			{
				minSoFar = myArray[i];
			}
		}
		// Console.WriteLine("Max value: " + maxSoFar);
		// Console.WriteLine("Min value: " + minSoFar);
		// Console.ReadLine();
		cout << "Max value: " << maxSoFar << endl;
		cout << "Min value: " << minSoFar << endl;
		cin.get();

		// Return 0 if successful
		return 0;
	}
// }

