#include <iostream>
#include <string>
#include <stdexcept> // standard except header - provides us with several different types of exceptions
using namespace std;

// Pass by value
void PassByValue(int value)
{
	// Make a copy of whatever value we pass in inside of this new variable (value)
	// Changes to "value" do not persist beyond this function's lifespan
	value = 1;
}

// Pass by reference
// Uses reference keycode - & = get address of
void PassByReference(int& refVariable)
{
	// Directly change whatever our input was using the refVariable - sort of like using an alias
	refVariable = 2;
}

// Pass by reference using pointers
void PassByReferenceUsingPointer(int* ptr)
{
	// ptr points to some integer - we need to dereference it to use it
	// This accomplishes the same thing as pass by reference

	// We need to handle the possibility of a null pointer.
	// Option 1: Do nothing. This is valid if the commands in this function aren't that important
	/*
	if (ptr == nullptr)
	{
		return;
	}
	*/

	// Option 2: Print a message.
	/*
	if (ptr == nullptr)
	{
		cout << "Error: cannot pass a null pointer by reference." << endl;
		return;
	}
	*/

	// Option 3: "Fix" the problem.
	/*
	int hackyFix = 0;
	if (ptr == nullptr)
	{
		// Just set the pointer to a different integer. EXTREMELY hacky
		ptr = &hackyFix;
	}
	*/

	// Option 4: Throw an exception.
	if (ptr == nullptr)
	{
		// An exception indicates an exceptional error - one that we cannot handle. We need to break out of the normal flow of the program to handle it
		// Immediately ends the function without returning anything. Propogates up the call stack until it finds a catch block. If none is found, the program ends.
		// throw 1234;

		// But instead of throwing an int, we can throw a runtime error (stdexcept)
		throw runtime_error("PassByReferenceUsingPointer() was given a null pointer");
	}

	*ptr = 3;
}

int& GetValueAtIndex(int* array, int size, int index)
{
	// If array is null throw an exception
	if (array == nullptr)
	{
		throw invalid_argument("GetValueAtIndex() was given a null array");
	}
	// If there is an overflow throw an exception
	if (index >= size)
	{
		throw out_of_range("GetValueAtIndex() was given an index outside the bounds of the array");
	}
	// If index is negative throw an exception
	if (index < 0)
	{
		throw out_of_range("GetValueAtIndex() was given a negative index");
	}
	// Using this many if statements per-CPU cycle can be extremely intensive and slow down the program. Can it be worth it to ignore all errors?

	// Simply return the value at this index.
	return array[index];
}

// Print a string by value - makes a copy of the string passed in
void PrintString(string s)
{
	cout << s << endl;
}
// Print a string by reference - no copy made, but we can't pass literals and we can change the string...
void PrintStringRef(string& s)
{
	cout << s << endl;
}
// Print a string by a constant reference - lets the code choose what to do. If a literal is passed in, it will pass by value - if a variable is passed in, it will pass by reference
void PrintStringConstRef(const string& s)
{
	cout << s << endl;
}

int main(int argc, char* argv[])
{
	// Inserts 56 into the function, which gets changed to 1. Nothing happens otherwise
	PassByValue(56);
	int myInt = 5;
	// Expects a reference - a variable, not a constant. myInt will get changed to 2
	PassByReference(myInt);
	// This needs to reference some integer somewhere - cannot be declared without referencing another var
	int& myIntReference = myInt; // This is an alias - the same memory with two names

	// Store a new int in memory
	int* myIntPtr = new int;
	// Need to pass in a pointer
	PassByReferenceUsingPointer(&myInt);
	PassByReferenceUsingPointer(myIntPtr);

	// Pass a null pointer in - this will point to 0 in memory and store 3 there... bad idea. Will throw an error if left unchecked
	// ...but what if we don't want the program to crash? What if there error isn't that serious?
	// So we will try it instead - detect when an exception occurs and use a catch block to handle it
	try
	{
		PassByReferenceUsingPointer(nullptr);
	}
	catch (int errorCode)
	{
		cout << "An exception occurred with error code " << errorCode << "." << endl;

		if (errorCode == 4567)
		{
			throw;
		}
	}
	// If it's an exception type, it will be caught here
	catch (exception& e)
	{
		// Return whatever occurred
		cout << "An exception occurred: " << e.what() << endl;
	}

	delete myIntPtr;

	// More references
	int array[] = { 78, 54, 109 };
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;

	// Weird, but we can do this because this reference represents the actual integer variable in that array. We are changing the value of the array through a function
	GetValueAtIndex(array, 3, 1) = 14;
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;

	try
	{
		// Oh god oh f*ck
		GetValueAtIndex(nullptr, 3, 1);
		GetValueAtIndex(array, 3, 5);
		GetValueAtIndex(array, 3, -12);
	}
	catch (exception& e)
	{
		cout << "An exception occurred: " << e.what() << endl;
	}

	// Test our PrintString by value function
	PrintString("This is a string.");
	// But what if it's really really big? We don't want to copy it...
	PrintString("According to all known laws of aviation, there is no way a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway because bees don\'t care what humans think is impossible. Yellow, black. Yellow, black. Yellow, black. Yellow, black. Ooh, black and yellow! Let\'s shake it up a little. Barry! Breakfast is ready! Coming! Hang on a second. Hello? Barry? Adam? Can you believe this is happening? I can\'t. I\'ll pick you up. Looking sharp. Use the stairs.Your father paid good money for those. Sorry.I\'m excited. Here\'s the graduate. We\'re very proud of you, son. A perfect report card, all B\'s. Very proud.");
	
	// We can establish a var and use it. We cannot pass string literals by reference
	string myString = "I can make a reference to this variable.";
	PrintStringRef(myString);

	// Trying out the const ref
	PrintStringConstRef(myString);
	PrintStringConstRef("Test string!");

	cin.get();
	return 0;
}