#include <iostream>
using namespace std;

int main(int argc, char* argv)
{
	// Pointer arithmetic - directly modifying memory addresses
	int* myArray = new int[10];
	for (int i = 0; i < 10; i++)
	{
		myArray[i] = i * 3;
	}
	// Print myArray before modification
	cout << "My Array: ";
	for (int i = 0; i < 10; i++)
	{
		cout << myArray[i] << " ";
	}
	cout << endl;

	// Point to the memory that myArray is pointing to
	int* a = myArray;
	// Asterisk dereferences and prints out the actual value that 'a' points to
	cout << "Starting value of 'a': " << *a << endl;
	cout << "Memory address of 'a': " << a << endl;

	// We can modify this pointer directly. Add one to 'a'
	a++;
	// 'a' is now 4 bytes ahead, because integers take up 4 bytes
	cout << "Second value of 'a': " << *a << endl;
	cout << "Second memory address of 'a': " << a << endl;

	a += 2;
	cout << "Fourth value of 'a': " << *a << endl;
	cout << "Fourth memory address of 'a': " << a << endl << endl;


	// Point to the start and the end of myArray
	int* start = myArray;
	int* end = myArray + 10;

	cout << "Printing region of memory with pointer arithmetic: ";
	// Initialize the loop on the start of the array, end it when we get to the end of the array
	for (int* p = start; p != end; p++)
	{
		cout << *p << " ";
	}
	cout << endl << endl;


	// Pointer arithmetic is why the following doesn't work - C++ interprets the + as "add" instead of "concatinate"
	int x = 6;
	while (true)
	{
		// Fish around in memory. A to go back, D to go forward, B to continue
		char c = cin.get();

		if (c == 'a')
		{
			x--;
		}
		else if (c == 'd')
		{
			x++;
		}
		else if (c == 'b')
		{
			break;
		}

		cout << "Hello" + x << endl;
	}


	// We are done with myArray
	delete[] myArray;

	cin.get();
	return 0;
}