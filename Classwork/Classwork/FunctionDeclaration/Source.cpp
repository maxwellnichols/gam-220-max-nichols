#include <iostream>
// Include our PrintJon.h header file to give us access to the function
// Double quotes means that it's a file in your project, angle brackets mean a file built into the compiler
#include "PrintJon.h"
using namespace std;

int main(int argc, char* argv[])
{
	// This function takes no arguments and returns nothing, but we don't know anything else about it. It might not even be a function at all - compiler does not know if a func is not defined
	// Here we are defining the function in PrintJon.h and storing it in PrintJon.cpp
	PrintJon();

	cin.get();
	return 0;
}