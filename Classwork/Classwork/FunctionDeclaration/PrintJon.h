#pragma once

// Forward declaration - tells the program that this function exists, but not to use it yet. It exists in a different cpp file in this case
// Without this, the program won't know what PrintJon is.
void PrintJon();