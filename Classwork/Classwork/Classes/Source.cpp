#include <iostream>
#include <string>
#include "Health.h"
using namespace std;

// Cannot have two classes with same name
/*
class MyClass
{
	float a;
};

class MyClass
{
	string fakfsfa;
};
*/

int main(int argc, char* argv[])
{
	// C#:
	// Health myHealth = new Health();

	int myNum; // This int is declared and allocated on the stack. This is automatically cleaned up when this function returns (for short-lived, small)
	Health myHealth; // Instantiate an object and call the constructor. This also gets cleaned up when this function returns

	int* yourNum = new int(); // This will be allocated on the heap (RAM). "new" always requests memory to be allocated to the heap (for long-lived, big)
	Health* yourHealth = new Health(); // "Allocate enough memory to store a health object, and then tell me where it is"

	// Accessing members of stack-allocated objects
	myHealth.TakeDamage(4);
	myHealth.Heal(2);

	// Accessing members of heap-allocated objects
	// Heap allocated objects are referred to by a pointer - we must dereference it.
	(*yourHealth).TakeDamage(7); // Use parentheses to explicitly dereference the object. Gross
	yourHealth->Heal(5);         // Or use an arrow instead. Better

	cin.get();

	// We have to explicitly free the memory used by the objects when we're done using them
	delete yourNum;
	delete yourHealth;

	return 0;
}