// This header is for the interface of Health
#pragma once // makes sure that this file only gets included one time
#include <iostream>
#include <string>

using namespace std;

/*	Alternatively:
	using std::string;
	using std::cout;
	using std::endl;*/

class Health
{
	// In a class: functions (behaviors) and variables (values)
	// In C++, this is how we declare private/public variables and functions. They must be grouped.
	// If not declared, classes default to private, while structs default to public
public:
	string name; // This string doesn't contain anything - it's still a string, but it has no information

	// char board[]; // This doesn't work because we don't know how much room this array will need
	// char board[3]; // Now we know how big it is but initialization doesn't work
	// We know that the array is just 3 characters in a row, we just need to know where they are in memory
	char* board; // We just need to store a memory address with a pointer, which we know the size of

	// Constructors do not have a return type and have the same name as the class
	// If we ever create a Health object, this gets called immediately
	Health();

	// Destructor
	~Health();

	void TakeDamage(int amount);

	void Heal(int amount);

	int GetHitPoints();

private:
	int hitPoints;

};  // C++ needs a semicolon after class definition