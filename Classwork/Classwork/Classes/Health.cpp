// This cpp is for the implementations of Health
#include "Health.h" // We need to tell this cpp file that Health.h is in its scope

Health::Health()
	: hitPoints(10), name("Jon")/*, board{'1', '2', '3'}*/ // initializer list - use this for more complex types
{
	cout << "Hitpoints uninitialized: " << hitPoints << endl;
	cout << "Name: \"" << name << "\"" << endl;

	// Declare hitpoints on object creation so we don't get garbage data
	// hitPoints = 10;
	// name = "Jon";

	// board = { '1', '2', '3' }; // This doesn't work, see Health.h
	board = new char[3]{ '1', '2', '3' }; // Uses the pointer version of board to init the array. We must delete this later...
}

// Destructor
Health::~Health()
{
	// Delete all of the elements in board
	delete[] board;
}

// Tell the compiler that TakeDamage is a function in the Health scope
void Health::TakeDamage(int amount)
{
	hitPoints -= amount;
}

void Health::Heal(int amount)
{
	hitPoints += amount;
}

int Health::GetHitPoints()
{
	return hitPoints;
}