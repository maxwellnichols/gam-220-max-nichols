#include <iostream>
#include <vector> // Gives us access to the Vector class
using namespace std;

int main(int argc, char* argv[])
{
	// List in C# = vector in C++
	// Declaring a vector
	vector<int> myVector;

	// Add an element onto the end of this container - 5, 23, 4
	myVector.push_back(5);
	myVector.push_back(23);
	myVector.push_back(4);

	// Print out the vector's contents
	cout << "Contents of vector: ";
	for (int i = 0; i < myVector.size(); i++)
	{
		cout << myVector[i] << " ";
	}
	cout << endl;

	// Traversal
	cout << "Traversal using an iterator: ";
	// Point at the first element in the vector, and continue going as long as iterator is not at the end of the vector
	// The iterator is an object declared in the vector class
	for (vector<int>::iterator iter = myVector.begin(); iter != myVector.end(); iter++)
	{
		// Use a pointer to grab the current value of the iterator
		cout << *iter << " ";
	}
	cout << endl;


	cin.get();
	return 0;
}