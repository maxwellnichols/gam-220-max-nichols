#include <iostream>;
// Double just kidding we can still use strings (standard namespace)
#include <string>;
using namespace std;

int main(int argc, char* argv[])
{
	// We don't have a string data type, so we need to make an array of characters
	char myString[] = {'H', 'e', 'l', 'l', 'o', '!'};

	for (int i = 0; i < 6; i++)
	{
		cout << myString[i];
	}
	cout << endl;

	// Just kidding that would suck. Use const char* instead
	// The value of myString2 is not allowed to change, due to the const declaration
	const char* myString2 = "Hello!"; // This variable ends with an invisible null terminator byte - marks end of the "string"
	cout << myString2 << endl;

	// What happens when there is no null terminator byte?
	char myString3[6];

	for (int i = 0; i < 6; i++)
	{
		myString3[i] = myString[i];
	}
	cout << "Without a null byte: " << myString3 << endl;
	// It keeps on going until it finds a null byte in memory... can be a security issue!

	// string has a .size() function like C#
	string myString4 = "This is a string object in C++";

	cout << myString4 << endl;
	cout << "This string has a size of " << myString4.size() << endl;

	// C# - get a line of input from the user
	// C# string input - Console.ReadLine();

	// cin.getline() - bad, have to declare length
	// use just getline() instead
	// cin is where getline() gets the input from
	// myString4 is where the input is stored
	getline(cin, myString4);

	cout << "You typed: " << myString4 << endl;

	cin.get();
	return 0;
}