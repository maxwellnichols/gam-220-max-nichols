#include <iostream>
using namespace std;

int main(int argc, char* argv)
{
	int a = 0;
	float b = 0.0f;
	// Plain old data (POD) - simple way to copy values. Store the value of 5 in a
	a = 5;

	// Non-POD types...?
	int* array1 = new int[10];
	// Initialize array values (0 - 27, 3 at a time)
	for (int i = 0; i < 10; i++)
	{
		array1[i] = i * 3;
	}

	// Set array 2 equal to array 1
	// This is a shallow copy - only the memory addresses have been copied, not any elements in the array
	int* array2 = array1;
	// If we change array 1, it will change array 2 as well since they both point to the same memory
	array1[3] = 999;
	// Print the values of Array 2
	cout << "Array 2: ";
	for (int i = 0; i < 10; i++)
	{
		cout << array2[i] << " ";
	}
	cout << endl;

	// Instead we will do a deep copy
	int* array3 = new int[10];
	// Copy each individual element from array 1 into array 3
	for (int i = 0; i < 10; i++)
	{
		array3[i] = array1[i];
	}
	// Now when we change array 1, it does not change array 3 since we directly copied the values instead of the memory addresses
	array1[4] = 1234;

	cout << "Array 3: ";
	for (int i = 0; i < 10; i++)
	{
		cout << array3[i] << " ";
	}
	cout << endl;

	cout << "Array 1: ";
	for (int i = 0; i < 10; i++)
	{
		cout << array1[i] << " ";
	}
	cout << endl;

	// We are done with array 1 and array 3
	delete[] array1;
	delete[] array3;

	cin.get();
	return 0;
}