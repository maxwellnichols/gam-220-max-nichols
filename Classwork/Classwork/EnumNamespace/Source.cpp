#include <iostream>
using namespace std;

// enum - enumeration: counting things using a set of objects
// scoped enum (preferred way, this is how they work in C#)
// switch/case/break statements
// namespace keyword

// Declaring an enum. This dumps them into the global scope
// enum SuccessEnum
// We are going to scope the enum instead to make it clear and avoid naming collisions
enum class SuccessEnum
{
	// Which values are possible?
	FAIL,
	SUCCESS,
	UNKNOWN
	// THISWILLBEREPLACEDBYTHENUMBERTHREEWHENCOMPILED
};
enum MyOtherEnum
{
	GREAT,
	GOOD,
	FAIL // This FAIL will trip the program up when mentioned when using global enums - which one does it use?
};

// Function that returns an enum
SuccessEnum DoSomeWork()
{
	cout << "Doing some work..." << endl;
	return SuccessEnum::SUCCESS; // We have to scope to the correct enum to use using ::
}

// Declaring a namespace. When calling this stuff we need to declare the scope
// This is useful in big projects, not so much smaller ones
namespace EnemyStuff
{
	// Enemy type enumeration
	enum EnemyType
	{
		GOBLIN,
		ORC,
		TROLL,
		DEMON
	};
	// This class will make use of the above enumeration
	class Enemy
	{
	public:
		int hitpoints;
		EnemyType type;
	};
}

// Create a subclass of Enemy, and in its constructor we can set the enum equal to one of its values
class Goblin : public EnemyStuff::Enemy
{
public:
	Goblin()
	{
		hitpoints = 5;
		type = EnemyStuff::GOBLIN; // If you ever do this, note that there is probably a better way
	}
};

int main(int argc, char* argv[])
{
	// Setting up an enum variable
	SuccessEnum workResult = DoSomeWork();

	// We can cast from an int to an enum, and vice versa
	SuccessEnum myValue = SuccessEnum::SUCCESS;
	int myNumber = (int)myValue; // Converts SUCCESS to 1
	myValue = (SuccessEnum)myNumber; // Converts 1 to SUCCESS (needs to know how big the enum is) 

	// Can use enums in logic statements
	if (workResult == SuccessEnum::SUCCESS)
	{
		cout << "Work was successful!" << endl;
	}
	else if (workResult == SuccessEnum::FAIL)
	{
		cout << "Work failed..." << endl;
	}
	else
	{
		cout << "What happened?" << endl;
	}

	myNumber = 10;
	// Switch/case/break works like if/else if/else, but instead we specify what values are possible, which allows it to "jump" to the proper case and makes it faster
	switch (myNumber)
	{
	case 0:
		cout << "0" << endl;
		break; // break is "optional," but then the execution will drop through to the next case's statements
	case -1: // This will drop through case 1 and end at the break. C# does not allow this
	case 1:
		cout << "1" << endl;
		break;
	case 10:
		cout << "10" << endl;
		break;
	// Equivalent to "else"
	default:
		cout << "That value is not 0, 1, or 10." << endl;
		break;
	}

	// More clear and less error prone than the if/else if approach
	switch (DoSomeWork())
	{
	case SuccessEnum::SUCCESS:
		cout << "Work was successful!" << endl;
		break;
	case SuccessEnum::FAIL:
		cout << "Work failed..." << endl;
		break;
	case SuccessEnum::UNKNOWN:
		cout << "What happened?" << endl;
		break;
	}




	cin.get();
	return 0;
}