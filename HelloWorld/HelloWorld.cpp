// # indicates special directive - used heavily in C++
// Preprocessor replaces this line with that file
#include <iostream>

// Specifies how we access these objects
// Namespaces are a form of organizing code
using namespace std;
// Now we don't have to type std::<object> every time we reference something in iostream

// main is the entrypoint for a C++ program
// main must return an integer (int argc) - name doesn't matter
// argv[] is an array, char* is special
int main(int argc, char* argv[])
{
	// Output Hello World! and then end the line
	// "Hello World!" is a string literal
	// Arrow << sending data to object
	cout << "Hello World!" << endl;
	
	// Get console input - in this case, waiting for a key input
	cin.get();
	
	// We need to return an integer - do it here
	return 0;
}