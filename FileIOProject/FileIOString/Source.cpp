#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
using namespace std;

int main(int argc, char* argv[])
{
	// Create the filestream object
	ifstream fin("textFile.txt");
	string input;
	int wordCount = 0;

	// Output the text file.
	cout << "IMPORTED TEXT FROM textFile.txt: \"";

	// While the interpretation is good, keep going through the text file.
	while (fin.good())
	{
		// The input operator will automatically find spaces, so by default we will just be importing each distinct word.
		fin >> input;
		cout << input << " ";
		// We found a word. Increase the word count by one.
		wordCount++;
	}
	cout << "\"" << endl << endl;

	// Output number of words.
	cout << "This text file has " << wordCount << " word";
	// Formatting
	if (wordCount == 1)
	{
		cout << "." << endl;
	}
	else
	{
		cout << "s." << endl;
	}

	cin.get();
	return 0;
}