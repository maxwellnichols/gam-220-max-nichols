#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
using namespace std;

int main(int argc, char* argv[])
{
	// We will need an array of 5 numbers
	int dataArray[5];
	string input;

	// Ask for an input for each of these numbers...
	for (int i = 0; i < 5; i++)
	{
		cout << "Please enter integer " << (i + 1) << ": ";
		getline(cin, input);

		try
		{
			// Only do this if we actually can convert the string to an int
			dataArray[i] = stoi(input);
		}
		catch (exception& e)
		{
			// String was not able to be converted to an int. Decrement the counter so we can try again.
			cout << "Error: input is not an integer." << endl;
			i--;
		}
	}
	// Find the sum
	int sum = 0;
	for (int i = 0; i < 5; i++)
	{
		sum += dataArray[i];
	}
	// Find the average - we must make sum a float or else it won't convert correctly
	float average = (float)sum / 5;

	// We have all of the data. Now we need to write it to the file
	ofstream fout("userData.txt");

	// Output each element on the same line
	fout << "Array Data: ";
	for (int i = 0; i < 5; i++)
	{
		fout << dataArray[i];
		fout << " ";
	}
	fout << endl;

	// Output the sum...
	fout << "Sum: " << sum << endl;
	fout << "Average: " << average << endl;

	// Close the file
	fout.close();

	// We're done here
	cout << endl << "Your data has been recorded." << endl;

	cin.get();
	return 0;
}