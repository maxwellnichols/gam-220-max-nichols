// Keeps track of what our vector's functions entail
// We need to make sure that this file only gets compiled once. #pragma once doesn't work - so we must use a hackier method
#ifndef __VECTOR_INL__ // Check if this symbol is not defined - if it isn't defined, define it and go through the whole file
#define __VECTOR_INL__ // Define this symbol...

#include "Vector.h"
#include <stdexcept> // We need to be able to throw exceptions

template<typename T> // We need to include this line before every function so that the compiler knows what T is
Vector<T>::Vector()
{
	// Default (and only) constructor - this will always be called. Let's set maximum elements to 10 to start
	vectorPointer = new T[10];

	maximumElements = 10;
}

template<typename T>
Vector<T>::~Vector()
{
	// Delete our data pointers
	delete[] vectorPointer;
}

template<typename T>
void Vector<T>::PushFront(T value)
{
	// We don't have enough space! Make room
	if (currentElements >= maximumElements)
	{
		Reserve(currentElements + 1);
	}

	// Move everything else up one
	for (int i = currentElements + 1; i >= 1; i--)
	{
		vectorPointer[i] = vectorPointer[i - 1];
	}
	// Now we can set the first element to the requested value
	vectorPointer[0] = value;
	currentElements++;
}

template<typename T>
void Vector<T>::PushBack(T value)
{
	// We don't have enough space! Make room
	if (currentElements >= maximumElements)
	{
		Reserve(currentElements + 1);
	}
	
	// currentElements tells us how many elements we have stored. Since we start at 0, we can just do this. Increment it after so we never overlap.
	vectorPointer[currentElements] = value;
	currentElements++;
}

template<typename T>
T& Vector<T>::At(int index)
{
	// If the index is out of range, catch that
	if (index > currentElements || index < 0)
	{
		throw out_of_range("At(): Requested index is outside of the range of the vector.");
	}

	return vectorPointer[index];
}

template<typename T>
T& Vector<T>::operator[](int index)
{
	// This just allows us to use square brackets to get a reference to a value instead of having to call At()

	// If the index is out of range, catch that
	if (index > currentElements || index < 0)
	{
		throw out_of_range("Operator Overload []: Requested index is outside of the range of the vector.");
	}

	return vectorPointer[index];
}

template<typename T>
void Vector<T>::Clear()
{
	// Create a new array with the correct number of elements. Make the vector pointer equal to it, and set currentElements to 0
	int* clearVector = new int[maximumElements];

	delete[] vectorPointer;
	vectorPointer = clearVector;
	currentElements = 0;
}

template<typename T>
int Vector<T>::Size()
{
	// Return how many elements the vector is holding
	return currentElements;
}

template<typename T>
bool Vector<T>::IsEmpty()
{
	// If there are no elements, the vector is empty.
	if (currentElements <= 0)
	{
		return true;
	}
	// There are elements in the vector, so it is not empty.
	else
	{
		return false;
	}
}

template<typename T>
void Vector<T>::Resize(int newSize)
{
	/*
	// We know what the initial size of the array are and what array we're using - we are recieving the requested size from the user
	// Create a place to temporarily store our vector's values
	int* resizedVector = new int[newSize];

	// We need to support both growing and shrinking the vector. Deep copy from our vector to the new array - use the smaller of the two size inputs so we don't overflow
	if (currentElements < newSize)
	{
		for (int i = 0; i < currentElements; i++)
		{
			resizedVector[i] = vectorPointer[i];
		}
	}
	else if (newSize < currentElements)
	{
		for (int i = 0; i < newSize; i++)
		{
			resizedVector[i] = vectorPointer[i];
		}
		// If we make the array smaller than the number of elements we are storing, that means that we HAVE TO BE at capacity now
		currentElements = newSize;
	}
	// Else do nothing because the requested size IS the current size
	else
	{
		// Yeet this thing out of here
		delete[] resizedVector;
		return;
	}

	// Update maximumElements
	maximumElements = newSize;
	// Delete the original vector and make it point to the resized one
	delete[] vectorPointer;
	vectorPointer = resizedVector;
	*/

	// Resize means we resize the vector in a messy way
	// Jon's code
	if (newSize < maximumElements)
	{
		currentElements = newSize;
		maximumElements = newSize;
	}
	else if (newSize > maximumElements)
	{
		Reserve(newSize);
		currentElements = newSize;
	}
}

template<typename T>
void Vector<T>::Reserve(int newCapacity)
{
	/*
	// If the new capacity is larger than our current capacity, set our current capacity to the new capacity.
	if (newCapacity > maximumElements)
	{
		// I guess just resize it? We need to actually update how many entries the vector has. If we just update maximumElements we run into heap corruption
		Resize(newCapacity);
	}
	// If it is smaller than our current capacity, do nothing.
	*/
	/*
	// Jon's code
	if (maximumElements < newCapacity)
	{
		int* newArray = new int[newCapacity];
		for (int i = 0; i < currentElements; i++)
		{
			newArray[i] = vectorPointer[i];
		}
		delete[] vectorPointer;
		vectorPointer = newArray;
		maximumElements = newCapacity;
	}
	*/

	// Reserve means that we resize the vector in a clean way.
	if (maximumElements < newCapacity)
	{
		T* resizedVector = new T[newCapacity];

		for (int i = 0; i < currentElements; i++)
		{
			resizedVector[i] = vectorPointer[i];
		}

		// Update maximumElements
		maximumElements = newCapacity;
		// Delete the original vector and make it point to the resized one
		delete[] vectorPointer;
		vectorPointer = resizedVector;
	}
}

template<typename T>
int Vector<T>::GetCapacity()
{
	// Return our current capacity
	return maximumElements;
}

template<typename T>
void Vector<T>::EraseAt(int index)
{
	// This is just like Erase - but we do not need to iterate through the vector to find out where to start, as an index is already given
	for (int i = index; i < currentElements - 1; i++)
	{
		vectorPointer[i] = vectorPointer[i + 1];
	}

	currentElements--;
	return;
	// TODO check for invalid index
}

template<typename T>
void Vector<T>::Erase(T value)
{
	// Remove first matching value from the vector
	// Loop through each possible element
	for (int i = 0; i < currentElements; i++)
	{
		// If we found it at this index...
		if (vectorPointer[i] == value)
		{
			// Set the value of the current element to the value of the one that is one index higher. Removes gaps that would exist from deleting data - and resizes the vector
			// Start the counter at the current element because we don't want to modify anything before it
			for (int j = i; j < currentElements - 1; j++)
			{
				vectorPointer[j] = vectorPointer[j + 1];
			}

			// Deincrement current element counter (to keep track) and return, we're done here
			currentElements--;
			return;
		}
		// We did not find it at this index...
	}
	// We did not find a matching value. Do nothing
}

template<typename T>
int Vector<T>::Find(T value)
{
	// Loop through the vector until we find the target value, and then return the index
	for (int i = 0; i < currentElements; i++)
	{
		// Did we find it here?
		if (vectorPointer[i] == value)
		{
			// Yep, here it is
			return i;
		}
		// Nope, keep looking
	}
	// Nope, does not exist in this vector
	return -1;
}

template<typename T>
bool Vector<T>::Contains(T value)
{
	// Loop through the vector to see if the target value exists
	for (int i = 0; i < currentElements; i++)
	{
		// Did we find it here...?
		if (vectorPointer[i] == value)
		{
			// Yep, it exists
			return true;
		}
		// Nope, not here
	}
	// Nope, it doesn't exist in this vector
	return false;
}

template<typename T>
void Vector<T>::Insert(T value, int index)
{
	// We don't have enough space! Make room
	if (currentElements >= maximumElements)
	{
		Reserve(currentElements + 1);
	}
	
	// Start at the given index and move everything else up one
	for (int i = currentElements; i > index; i--)
	{
		vectorPointer[i] = vectorPointer[i - 1];
	}
	// Now we can set the first element to the requested value
	vectorPointer[index] = value;
	currentElements++;
}

#endif // End the ifdef from above. If that symbol is already defined, then nothing in this file will be included