#include <iostream>
#include <string>
#include "Vector.h"
using namespace std;

int main(int argc, char* argv[])
{
	Vector<int> testVector;

	for (int i = testVector.Size(); i < testVector.GetCapacity(); i++)
	{
		testVector.PushBack(i * 4);
	}

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;

	cout << "Finding and erasing the 24 [Erase()]..." << endl;
	testVector.Erase(24);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;

	cout << "Erasing element 3 [EraseAt()]..." << endl;
	testVector.EraseAt(3);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	cout << "Pushing 97 into first slot [PushFront()]..." << endl;
	testVector.PushFront(97);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	try
	{
		cout << "Requesting reference to element 5 [At()]: " << testVector.At(5) << endl;
		cout << "Trying element 500 [At()]..." << endl;
		cout << "Requesting reference to element 500 {At()]: " << testVector.At(500) << endl;
	}
	catch (exception& e)
	{
		cout << "An exception occurred: " << e.what() << endl;
	}
	cout << endl;

	cout << "Inserting 82 into the position 4 [Insert()]..." << endl;
	testVector.Insert(82, 4);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	if (testVector.IsEmpty())
	{
		cout << "Test Vector is empty! [IsEmpty()]" << endl;
	}
	else
	{
		cout << "Test Vector is not empty. [IsEmpty()]" << endl;
	}

	cout << "Clearing the Test Vector [Clear()]..." << endl;
	testVector.Clear();

	if (testVector.IsEmpty())
	{
		cout << "Test Vector is empty! [IsEmpty()]" << endl;
	}
	else
	{
		cout << "Test Vector is not empty. [IsEmpty()]" << endl;
	}
	cout << endl;

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	cout << "Pushing a few new elements to Test Vector [PushBack(), PushFront(), Insert()]..." << endl; // should show up as 69 7 25 42
	testVector.PushBack(7);
	testVector.PushBack(42);
	testVector.PushFront(69);
	testVector.Insert(25, 2);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	cout << "Resizing Test Vector to 2 [Resize()]..." << endl;
	testVector.Resize(2);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;

	cout << "Maximum size of Test Vector [GetCapacity()]: " << testVector.GetCapacity() << endl;

	cout << "Resizing Test Vector to 6 [Resize()]..." << endl;
	testVector.Resize(6);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;

	cout << "Maximum size of Test Vector [GetCapacity()]: " << testVector.GetCapacity() << endl;

	cout << "Resizing Test Vector to 12 [Resize()]..." << endl;
	testVector.Resize(12);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl << endl;

	cout << "Maximum size of Test Vector [GetCapacity()]: " << testVector.GetCapacity() << endl;
	cout << "Current size of Test Vector [Size()]:        " << testVector.Size() << endl;

	cout << "Reserving four additional elements in Test Vector [Reserve()]..." << endl;
	testVector.Reserve(16);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;

	cout << "Maximum size of Test Vector [GetCapacity()]: " << testVector.GetCapacity() << endl;
	cout << "Current size of Test Vector [Size()]:        " << testVector.Size() << endl << endl;

	cout << "Pushing one more number to the front of the vector..." << endl;
	testVector.PushFront(777);

	cout << "Elements in Test Vector: ";
	for (int i = 0; i < testVector.Size(); i++)
	{
		cout << testVector.At(i) << " ";
	}
	cout << endl;
	cout << "Maximum size of Test Vector [GetCapacity()]: " << testVector.GetCapacity() << endl;
	cout << "Current size of Test Vector [Size()]:        " << testVector.Size() << endl << endl;

	cout << "Finding 777 in Test Vector: At index " << testVector.Find(777) << endl;
	cout << "Finding 7 in Test Vector:   At index " << testVector.Find(7) << endl;
	cout << "Finding 291 in Test Vector: At index " << testVector.Find(291) << endl << endl;

	cout << "Referencing entries of Test Vector with []: " << endl;
	try
	{
		cout << "Element 2: " << testVector[2] << endl;
		cout << "Element 1: " << testVector[1] << endl;
		cout << "Element 200?..." << endl;
		cout << testVector[200] << endl;
	}
	catch (exception & e)
	{
		cout << "An exception occurred: " << e.what() << endl;
	}
	cout << endl;

	cout << "----------------------------------------" << endl;

	// I need to see what happens here
	cout << "Shrinking a vector that isn't at capacity:" << endl << endl;
	Vector<int> thisVectorBreaksEverything;

	// Fill the first 6 values
	for (int i = thisVectorBreaksEverything.Size(); i < 6; i++)
	{
		thisVectorBreaksEverything.PushBack(i * 3);
	}

	// Output them...
	cout << "Elements in Broken Vector: ";
	for (int i = 0; i < thisVectorBreaksEverything.Size(); i++)
	{
		cout << thisVectorBreaksEverything.At(i) << " ";
	}
	cout << endl;
	cout << "Size of Broken Vector: " << thisVectorBreaksEverything.Size() << endl;
	cout << "Capacity of Broken Vector: " << thisVectorBreaksEverything.GetCapacity() << endl;

	// Resize it to 8. There are still only 6 elements
	cout << "Resizing Broken Vector to 8..." << endl;
	thisVectorBreaksEverything.Resize(8);

	cout << "Elements in Broken Vector: ";
	for (int i = 0; i < thisVectorBreaksEverything.Size(); i++)
	{
		cout << thisVectorBreaksEverything.At(i) << " ";
	}
	cout << endl;
	cout << "Size of Broken Vector: " << thisVectorBreaksEverything.Size() << endl;
	cout << "Capacity of Broken Vector: " << thisVectorBreaksEverything.GetCapacity() << endl;

	cout << "Amending one more element to Broken Vector..." << endl;
	thisVectorBreaksEverything.PushBack(100);

	cout << "Elements in Broken Vector: ";
	for (int i = 0; i < thisVectorBreaksEverything.Size(); i++)
	{
		cout << thisVectorBreaksEverything.At(i) << " ";
	}
	cout << endl;

	cout << "----------------------------------------" << endl;

	cout << "Jon's vector test suite:" << endl << endl;
	Vector<int> v;
	cout << "Size should be 0: " << v.Size() << endl;
	cout << "Capacity could be 10: " << v.GetCapacity() << endl;

	v.Reserve(20);
	cout << "Size should be 0: " << v.Size() << endl;
	cout << "Capacity should be 20: " << v.GetCapacity() << endl;

	v.Reserve(5);
	cout << "Size should be 0: " << v.Size() << endl;
	cout << "Capacity should be 20: " << v.GetCapacity() << endl;

	v.PushBack(4);
	cout << "Size should be 1: " << v.Size() << endl;
	cout << "Capacity should be 20: " << v.GetCapacity() << endl;

	for (int i = 0; i < 30; ++i)
	{
		v.PushBack(i);
	}
	cout << "Size should be 31: " << v.Size() << endl;
	cout << "Capacity should be at least 31: " << v.GetCapacity() << endl;

	v.Resize(10);
	cout << "Size should be 10: " << v.Size() << endl;
	cout << "Capacity should be at least 10: " << v.GetCapacity() << endl;

	v.Resize(15);
	cout << "Size should be 15: " << v.Size() << endl;
	cout << "Capacity should be at least 15: " << v.GetCapacity() << endl << endl;

	cout << "----------------------------------------" << endl;

	cout << "Quick test with a string vector..." << endl << endl;
	Vector<string> stringVector;

	// Push back some values
	stringVector.PushBack("Hey");
	stringVector.PushBack("Yo");
	stringVector.PushBack("Howdy");
	stringVector.PushBack("Hello");

	cout << "Initial String Vector..." << endl;
	// Output them
	for (int i = 0; i < stringVector.Size(); i++)
	{
		// Works with overloaded [] operator
		cout << stringVector[i] << " ";
	}
	cout << endl << endl;

	cout << "Modifying String Vector..." << endl;
	// Erase one
	stringVector.Erase("Yo");

	// Insert one
	stringVector.Insert("Hi", 2);

	// Output them again
	for (int i = 0; i < stringVector.Size(); i++)
	{
		// Works with overloaded [] operator
		cout << stringVector[i] << " ";
	}
	cout << endl << endl;

	cout << "Requesting references to String Vector..." << endl;
	try
	{
		cout << "Requesting reference to element 3 [At()]: " << stringVector.At(3) << endl;
		cout << "Trying element 400 [At()]..." << endl;
		cout << "Requesting reference to element 400 {At()]: " << stringVector.At(400) << endl;
	}
	catch (exception & e)
	{
		cout << "An exception occurred: " << e.what() << endl;
	}
	cout << endl;

	cout << "Finding things in String Vector..." << endl;
	cout << "Finding Hello in Test Vector: At index " << stringVector.Find("Hello") << endl;
	cout << "Finding Hey in Test Vector:   At index " << stringVector.Find("Howdy") << endl;
	cout << "Finding Yo in Test Vector: At index " << stringVector.Find("Yo") << endl << endl;

	cout << stringVector.Size() << "/" << stringVector.GetCapacity() << " slots used in String Vector." << endl;

	cout << "Resize down to 2..." << endl;
	stringVector.Resize(2);
	// Output them again
	for (int i = 0; i < stringVector.Size(); i++)
	{
		// Works with overloaded [] operator
		cout << stringVector[i] << " ";
	}
	cout << endl;
	cout << stringVector.Size() << "/" << stringVector.GetCapacity() << " slots used in String Vector." << endl;

	cout << "Reserving a few spaces in String Vector..." << endl;
	// Reserve a few spaces
	stringVector.Reserve(5);
	cout << stringVector.Size() << "/" << stringVector.GetCapacity() << " slots used in String Vector." << endl;

	cin.get();
	return 0;
}