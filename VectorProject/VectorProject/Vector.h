// Keeps track of what our vector can do
#pragma once
#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Vector
{
public:
	Vector();                          // Default constructor - DONE

	~Vector();                         // Default destructor - DONE

	void PushFront(T value);         // Add a value to the beginning of the container - should never fail - DONE

	void PushBack(T value);          // Add a value to the end of the container - should never fail - DONE

	T& At(int index);                // Return a reference to the element at this index - if unable, throw an exception - DONE

	// This will allow us to use square brackets (like an array) instead of having to call At()
	T& operator[](int index);        // Return a reference to the element at this index - if unable, throw an exception - ??

	void Clear();                      // Remove all elements from the container - DONE

	int Size();                        // Returns number of elements in the container - DONE

	bool IsEmpty();                    // Returns whether or not the container has any elements - DONE

	void Resize(int newSize);          // Adds or removes elements from the end of the container to achieve the new size - DONE

	void Reserve(int newCapacity);     // Allocates more room in the container. Does not shrink container - ?

	int GetCapacity();                 // Returns amount of allocated space - DONE

	void EraseAt(int index);           // Removes value at given index and decreases the container's size - DONE

	void Erase(T value);             // Remove the first value from the container that matches - DONE

	int Find(T value);               // Returns index of given value - if not found, returns -1 - DONE

	bool Contains(T value);          // Returns true if the value is in the container - DONE

	void Insert(T value, int index); // Insert given element at given position - 0 should insert element at the beginning of the container - should never fail - DONE

	// Variables
	T* vectorPointer;

	int currentElements = 0;

	int maximumElements;
};

#include "Vector.inl" // Templates are weird and don't get compiled the same way as normal classes. Include the template at the end of the header to ensure it gets built.
					  // Vector.cpp was turned into a .inl to make sure it doesn't get compiled by VS as a standard source file.