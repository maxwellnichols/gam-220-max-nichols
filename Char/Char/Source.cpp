#include <iostream>
#include <string>

using namespace std;

// Input a char, get out a char.
char ToLower(char input)
{
	// Check if the character is uppercase (between 65 and 90 inclusive on the ASCII table)
	if (input < 65 || input > 90)
	{
		// Not uppercase. Return lowercase input
		cout << "Input is not an uppercase letter: ";
		return input;
	}
	else
	{
		// Add 32 to go from uppercase to lowercase
		cout << "Lowercase version: ";
		return input + 32;
	}
}

// Input a char, get out a char again.
char ToUpper(char input)
{
	// Check if the character is lowercase (between 97 and 122 inclusive on the ASCII table)
	if (input < 97 || input > 122)
	{
		// Not lowercase. Return uppercase input
		cout << "Input is not a lowercase letter: ";
		return input;
	}
	else
	{
		// Subtract 32 to go from lowercase to uppercase
		cout << "Uppercase version: ";
		return input - 32;
	}
}

// Does not take any input params. Returns a string.
string PrintAlphabet()
{
	string alphabet;

	// Go through the ASCII list and add each letter to the final string
	for (int i = 65; i < 91; i++)
	{
		// Oh you can add chars to strings, cool
		alphabet += i;
	}

	return alphabet;
}

// Does not take any input params, returns a string again
string PrintAlphabetRandom()
{
	string randomAlphabet;

	// Create an array of chars - one for each letter
	char orderedAlphabet[26];

	for (int i = 65; i < 91; i++)
	{
		orderedAlphabet[i - 65] = i;
	}

	// We have our alphabet array. Now we just have to mix it up a few hundred times (using rand because we don't have anything better right now)
	// Temp storage value for swapping positions in the array
	char tempStorage;

	// Swap positions between two entries 100 times. Should be more than enough
	for (int i = 0; i < 100; i++)
	{
		// Generate two random numbers between 0 and 25
		int x = rand() % 26;
		int y = rand() % 26;

		// Swap the values in the randomly determined positions.
		tempStorage = orderedAlphabet[x];
		orderedAlphabet[x] = orderedAlphabet[y];
		orderedAlphabet[y] = tempStorage;
	}

	// Compile the array into a single string
	for (int i = 0; i < 26; i++)
	{
		randomAlphabet += orderedAlphabet[i];
	}

	return randomAlphabet;
}

int main(int argc, char* argv[])
{
	// Run through all the functions and output the results.
	// Uppercase to lowercase
	cout << "Enter an uppercase letter: ";
	char userInputToLower = cin.get();
	cout << ToLower(userInputToLower) << "\n" << endl;
	cin.get();

	// Lowercase to uppercase
	cout << "Enter a lowercase letter: ";
	char userInputToUpper = cin.get();
	cout << ToUpper(userInputToUpper) << "\n" << endl;

	// Ordered Alphabet
	cout << "Ordered Alphabet: " << PrintAlphabet() << endl << endl;

	// Random Alphabet
	cout << "Random Order A:   " << PrintAlphabetRandom() << endl;
	cout << "Random Order B:   " << PrintAlphabetRandom() << endl;
	cout << "Random Order C:   " << PrintAlphabetRandom() << endl;
	cin.get();

	cin.get();
	return 0;
}