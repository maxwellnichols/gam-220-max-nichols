#include <iostream>
#include <string>
#include "StringBag.h"
using namespace std;

int main(int argc, char* argv[])
{
	// Create a simple StringBag object on the stack
	cout << "USING DEFAULT CONSTRUCTOR" << endl;
	StringBag bag1;
	cout << "Bag 1 Size (should be 0): " << bag1.Size() << endl;                     // How many strings are in the bag
	cout << "Bag 1 Capacity (should be 10): " << bag1.GetCapacity() << endl << endl; // How many strings the bag can possibly hold

	// Create a second StringBag that uses a capacity argument
	cout << "USING ALTERNATIVE CONSTRUCTOR" << endl;
	StringBag bag2(27);
	cout << "Bag 2 Size (should be 0): " << bag2.Size() << endl;
	cout << "Bag 2 Capacity (should be 27): " << bag2.GetCapacity() << endl << endl;

	// Make sure that the destructor works
	cout << "TESTING DESTRUCTOR" << endl << endl;
	StringBag* bag3 = new StringBag();
	delete bag3;                                                                     // Delete calls a class's destructor

	// Test the "add" function
	cout << "ADDING \"Test 1\" TO BAG 1" << endl;
	bag1.Add("Test 1");
	// Print out all strings in bag 1 to make sure "Test 1" got added
	for (int i = 0; i < bag1.Size(); i++)
	{
		cout << "In Bag 1: " << bag1.At(i) << endl;
	}
	cout << endl << endl;

	// Test adding a string array to bag 2
	cout << "ADDING ARRAY OF STRINGS TO BAG 2" << endl;
	string stringArray[] = { "Array 1", "Array 2", "Array 3" };
	bag2.Add(stringArray, 3);
	// Print out all the strings in bag 2
	for (int i = 0; i < bag2.Size(); i++)
	{
		cout << "In Bag 2: " << bag2.At(i) << endl;
	}
	cout << endl << endl;

	// Test removing a string
	cout << "REMOVING \"Array 2\" FROM BAG 2" << endl;
	bag2.Remove("Array 2");
	// Print it all out to make sure
	for (int i = 0; i < bag2.Size(); i++)
	{
		cout << "In Bag 2: " << bag2.At(i) << endl;
	}
	cout << endl << endl;

	// Test the "contains" function to see whether a string exists or not
	if (bag2.Contains("Array 1"))
	{
		cout << "BAG 2 CONTAINS \"Array 1\"" << endl;
	}
	else
	{
		cout << "BAG 2 DOES NOT CONTAIN \"Array 1\"" << endl;
	}
	if (bag2.Contains("Error"))
	{
		cout << "BAG 2 CONTAINS \"Error\"" << endl;
	}
	else
	{
		cout << "BAG 2 DOES NOT CONTAIN \"Error\"" << endl;
	}
	cout << endl;

	// Test out find function
	cout << "FINDING \"Array 3\" IN BAG 2: " << bag2.Find("Array 3") << endl;         // Should work
	cout << "FINDING \"Wrong\" IN BAG 2: " << bag2.Find("Wrong") << endl;             // Should return -1
	cout << endl;

	// Test the CountOccurances function
	cout << "ADDING \"Array 3\" TO BAG 1 SEVERAL TIMES" << endl;
	bag1.Add("Array 3");
	bag1.Add("Array 3");
	bag1.Add("Array 3");
	cout << "\"Array 3\" OCCURS " << bag1.CountOccurrences("Array 3") << " TIMES" << endl;     // Should be 3
	cout << "\"Not Found\" OCCURS " << bag1.CountOccurrences("Not Found") << " TIMES" << endl; // Should be 0
	cout << endl;

	// Test the union function to combine StringBags
	cout << "UNION OF BAG 1 AND BAG 2" << endl;
	// Address of bag2, not bag2 itself
	bag1.Union(&bag2);
	// Print it all out to check
	for (int i = 0; i < bag1.Size(); i++)
	{
		cout << "In Bag 1: " << bag1.At(i) << endl;
	}
	cout << endl << endl;

	// Finally, take everything from bag2 and tack it on bag1, then print
	cout << "ADDING BAG 2 INTO BAG 1" << endl;
	bag1.Add(&bag2);
	for (int i = 0; i < bag1.Size(); i++)
	{
		cout << "In Bag 1: " << bag1.At(i) << endl;
	}
	cout << endl << endl;

	cin.get();
	return 0;
}