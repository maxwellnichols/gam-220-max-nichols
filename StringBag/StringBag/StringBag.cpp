// Use this cpp file to determine what a StringBag object actually does
#include "StringBag.h"

StringBag::StringBag()
{
	// We are using the default constructor, so maximum strings is 10
	stringBagPointer = new string[10];

	maximumStrings = 10;
}

StringBag::StringBag(int capacity)
{
	// We are using the alternative constructor, so maximum strings is the entered capacity
	stringBagPointer = new string[capacity];

	maximumStrings = capacity;
}

void StringBag::Add(string value)
{
	// We know how many strings the StringBag has at the moment (currentStrings) - we need to add one at its index and increment it by one so it doesn't get overwritten later
	// If the stringbag currently has 0 strings, write the first string at index 0. It just works
	stringBagPointer[currentStrings] = value;
	currentStrings++;
}

void StringBag::Add(string* array, int count)
{
	// Iterate though the array of strings to add and add them to the current string bag
	for (int i = 0; i < count; i++)
	{
		// Set the current string in the StringBag equal to the initial index of the imported array
		stringBagPointer[currentStrings] = array[i];
		// For each of these increment the counter by one
		currentStrings++;
	}
}

void StringBag::Add(StringBag* other)
{
	// A StringBag is basically just an array with extra steps so we can reuse the above function but make it work with a class
	for (int i = 0; i < other->currentStrings; i++)
	{
		stringBagPointer[currentStrings] = other->stringBagPointer[i];
		currentStrings++;
	}
}

int StringBag::Size()
{
	// Return the current value
	return currentStrings;
}

int StringBag::GetCapacity()
{
	// Just return the max value
	return maximumStrings;
}

void StringBag::Remove(string value)
{
	// Loop through the array and delete the first instance of the input value we see
	for (int i = 0; i < maximumStrings; i++)
	{
		// If we found it at this index
		if (stringBagPointer[i] == value)
		{
			// Set the value of the current element to the value of the one that is one index higher. This removes gaps that would exist from deleting data
			for (int j = i; j < maximumStrings - 1; j++)
			{
				stringBagPointer[j] = stringBagPointer[j + 1];
			}

			// Deincrement the current string count and return
			currentStrings--;
			return;
		}
		// Else try again...
	}
	// We didn't find it. Oh well
}

bool StringBag::Contains(string target)
{
	// Loop through the StringBag and see if the target is anywhere
	for (int i = 0; i < currentStrings; i++)
	{
		// Does it match?
		if (stringBagPointer[i] == target)
		{
			return true;
		}
		// Else continue loop
	}
	// Did not find a match
	return false;
}

string StringBag::At(int index)
{
	// Well this one is easy
	return stringBagPointer[index];
}

int StringBag::Find(string target)
{
	// Loop through the stringbag until we find the target, then return the index
	for (int i = 0; i < currentStrings; i++)
	{
		if (stringBagPointer[i] == target)
		{
			// Ladies and gentlemen we got em
			return i;
		}
		// Did not find it yet
	}
	// Did not find it in this stringbag
	return -1;
}

int StringBag::CountOccurrences(string target)
{
	// Loop through the StringBag and increment a counter whenever we come across the target entry
	int counter = 0;

	for (int i = 0; i < currentStrings; i++)
	{
		if (stringBagPointer[i] == target)
		{
			// Found it
			counter++;
		}
	}
	// Return the number of times we found it
	return counter;
}

void StringBag::Union(StringBag* other)
{
	// Loop through the other stringbag. On each loop check if the string is already in our current stringbag - if it isn't, add it
	for (int i = 0; i < other->currentStrings; i++)
	{
		// If it does not contain it...
		if (!Contains(other->stringBagPointer[i]))
		{
			Add(other->stringBagPointer[i]);
		}
	}
}

StringBag::~StringBag()
{
	// Delete all our data pointers
	delete[] stringBagPointer;
}