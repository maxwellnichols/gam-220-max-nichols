// Use this header file to keep track of what a StringBag object can do
#pragma once
#include <iostream>
#include <string>
using namespace std;

class StringBag
{
public:
	StringBag();             // Default constructor - room for 10 strings per bag

	StringBag(int capacity); // Allows us to specify the capacity of the StringBag

	void Add(string value);  // Add a single string to the container

	void Add(string* array, int count); // Adds an array of strings to the container

	void Add(StringBag* other); // Add the contents of another string bag to this one

	int Size();              // Returns the number of strings in this bag

	int GetCapacity();       // Returns amount of allocated space

	void Remove(string value); // Remove the first string that matches from the container

	bool Contains(string target); // Returns true if target string is in this bag

	string At(int index);    // Returns string at given index, or an empty string if not found

	int Find(string target); // Returns index of first matching string, or -1 if not found

	int CountOccurrences(string target); // Check how many times this string appears in this bag

	void Union(StringBag* other); // Add unique strings from another bag to this one

	~StringBag();            // Destructor

	// Variables
	string* stringBagPointer;

	int currentStrings = 0;

	int maximumStrings;
};