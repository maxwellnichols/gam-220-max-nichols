﻿using System;
using System.Diagnostics;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome to Speed Math!");
        Console.WriteLine("Your skills are about to be tested.");
        Console.WriteLine("Press Enter to begin...");
        Console.ReadLine();

        Random rnd = new Random();
        Stopwatch sw = new Stopwatch();
        int numCorrect = 0;
        int numWrong = 0;
        sw.Start();

        for (int i = 0; i < 10; ++i)
        {
            int x = rnd.Next(0, 10);
            int y = rnd.Next(0, 10);
            int operationIndex = rnd.Next(0, 4);
            int answer = 0;

            if (operationIndex == 0) // Add
            {
                Console.Write(x + " + " + y + " = ");
                answer = x + y;
            }
            else if (operationIndex == 1) // Subtract
            {
                Console.Write(x + " - " + y + " = ");
                answer = x - y;
            }
            else if (operationIndex == 2) // Multiply
            {
                Console.Write(x + " * " + y + " = ");
                answer = x * y;
            }
            else if (operationIndex == 3) // Divide
            {
                if(y == 0)
                    y = rnd.Next(1, 10);
                Console.Write(x + " / " + y + " = ");
                answer = x / y;
            }

            // Wait for input
            int guess;
            string input = Console.ReadLine();

            // Convert input into a value we can check
            while (!int.TryParse(input, out guess))
            {
                Console.WriteLine("Huh?");
                input = Console.ReadLine();
            }

            if (guess == answer)
            {
                numCorrect++;
            }
            else
            {
                numWrong++;
            }
        }
        sw.Stop();

        Console.WriteLine("You got " + numCorrect + " out of " + (numCorrect + numWrong));
        Console.WriteLine("Your Total Time: " + sw.Elapsed);

        if (numWrong == 0)
        {
            Console.WriteLine("That was perfect!!!");
        }
        else if (numWrong < numCorrect)
        {
            Console.WriteLine("You did fine.");
        }
        else if (numCorrect == 0)
        {
            Console.WriteLine("Are you okay?");
        }
        else
        {
            Console.WriteLine("That was pretty bad.  Try again!");
        }

        Console.ReadLine();
    }

}
