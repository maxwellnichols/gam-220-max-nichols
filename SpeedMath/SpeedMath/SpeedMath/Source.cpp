#include <iostream>
#include <string>
#include <chrono>
// stdlib.h and time.h are for the random number generator. I found this information at cplusplus.com/reference/cstdlib/rand/
#include <stdlib.h>
#include <time.h>

using namespace std;
using namespace chrono;

int main(int argc, char* argv[])
{
	// Program introduction
	cout << "Welcome to Speed Math!" << endl;
	cout << "Your skills are about to be tested." << endl;
	cout << "Press Enter to begin..." << endl;

	cin.get();

	// Keep track of scoring
	int numCorrect = 0;
	int numWrong = 0;

	// Random number generator seed, based on system time. I found this information at cplusplus.com/reference/cstdlib/rand/
	srand(time(NULL));
	
	// Start the timer.
	high_resolution_clock::time_point startTime = high_resolution_clock::now();

	// Quiz loop. Ask 10 questions, and then exit the loop.
	for (int i = 0; i < 10; i++)
	{
		// Generate two random variables between 0 and 9 inclusive. These are used for the math questions.
		int x = rand() % 10;
		int y = rand() % 10;
		// Generate another random number between 0 and 3 inclusive to determine what type of question we get.
		int operationIndex = rand() % 4;
		// Reset the answer at the start of each loop
		int answer = 0;

		if (operationIndex == 0) // Add
		{
			cout << x << " + " << y << " = ?" << endl;
			answer = x + y;
		}
		else if (operationIndex == 1) // Subtract
		{
			cout << x << " - " << y << " = ?" << endl;
			answer = x - y;
		}
		else if (operationIndex == 2) // Multiply
		{
			cout << x << " * " << y << " = ?" << endl;
			answer = x * y;
		}
		else if (operationIndex == 3) // Divide
		{
			// No dividing by 0. Re-roll y
			if (y == 0)
			{
				y = rand() % 9 + 1;
			}

			// Since all answers are ints, we need to handle non-divisible numbers.
			// Check if x is divisible by y. If x % y is not 0, the question is not valid.
			// This is probably a really bad way to do this, but re-roll x and y until x % y is 0
			while (x % y != 0)
			{
			  	x = rand() % 10;
			 	y = rand() % 9 + 1;
			}

			cout << x << " / " << y << " = ?" << endl;
			answer = x / y;
		}
		else // This should never happen
		{
			cout << "I don't know what you did, but you broke it. Nice...?" << endl;
			return 1;
		}

		// Wait for input. Establish the guess and the input string
		int guess;
		string input;
		getline(cin, input);

		cout << endl;

		// Convert the input to an integer
		// This crashes the program if the user doesn't input an integer, but we'll pretend that they won't do that (they will, they always will)
		guess = stoi(input);

		// Did the user get the answer right or wrong?
		if (guess == answer)
		{
			numCorrect++;
		}
		else
		{
			numWrong++;
		}
	}
	// End of quiz. Stop the timer
	high_resolution_clock::time_point stopTime = high_resolution_clock::now();
	// Find elapsed time. Convert to seconds
	milliseconds totalMS = duration_cast<milliseconds>(stopTime - startTime);
	// I will use a float instead of an int for extra precision in the timer because I think it looks nicer.
	float numSeconds = totalMS.count();
	numSeconds /= 1000;

	// Output results to user.
	cout << "You got " << numCorrect << " out of " << (numCorrect + numWrong) << " correct." << endl;
	cout << "Total time: " << numSeconds << " seconds." << endl;

	// Flavor text
	if (numWrong == 0)
	{
		cout << "That was perfect!" << endl;
	}
	else if (numWrong < numCorrect)
	{
		cout << "You did fine." << endl;
	}
	else if (numCorrect == 0) // Critical failure
	{
		cout << "...are you okay?" << endl;
	}
	else
	{
		cout << "That was pretty bad. Try again!" << endl;
	}

	cin.get();

	// Return 0 on successful completion
	return 0;
}