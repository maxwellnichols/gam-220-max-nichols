Max Nichols 
GAM 220 

1. While C# uses the Console.WriteLine() function, C++ uses cin. Console.WriteLine() can add strings together, but cin cannot; instead, it uses << operators. Console.WriteLine() is one command; while cin must end with "<< endl;".
2. stoi() stands for "string to integer." This function converts a string to an integer. This is useful for having the user input whole numbers.
3. The crono library provides a high precision clock. When "time_point startTime = now();" is declared, the timer starts, and when "time_point stopTime = now();" is declared, the timer stops. We can then get the number of milliseconds that passed between the timer starting and stopping, and convert that to an int or float for use in our program. In Speed Math, this will be used as the program's timer, to keep track of the user.
4. % is the modulo operator. If z = x % y, the program will set z equal to whatever the remainder of x / y is. This is useful for determining whether a number is a factor of another; for example, if x % y is 0, then we know that x fits perfectly into y.
5. cin.getline() requires us to know exactly how many characters the user will input, but std::getline() will work with any number of characters. cin.getline() is very bad.