#include <iostream>
#include <bitset> // so we can print these values out
using namespace std;

int main(int argc, char* argv[])
{
	// With bitwise operators, we can manipulate bits directly
	unsigned char c = 0b00001110;     // 14, c cannot be negative

	// Bitwise AND - &
	unsigned char d = c & 0b00000100; // Logical AND between each of these bits...
									  //   00001110
									  // & 00000100
								      // = 00000100 // Only becomes a 1 when both are 1s
									  // We can use this to store lots of different information in a single integer - just check if a bit was flipped
									  // Can single out a bit by doing an AND with a NOT

	// Bitwise OR - |
	unsigned char e = 0b00010010
		            | 0b01100110;
	// Expected result: 01110110 // Becomes a 1 if either is a 1

	// Bitwise XOR - ^
	unsigned char f = 0b00010010
				    ^ 0b01100110;
	// Expected result: 01110100 // Becomes a 1 if either is a 1, but not if both are 1. Every place that they are different becomes a 1

	// Bitwise NOT - ~ (unary)
	unsigned char g = ~0b00010010;
	// Expected result:  11101101 // Flips all bits

	// Right Shift Operator
	unsigned char h = 0b00010010 >> 1;                               // 18
	// Expected result: 00001001 // Shifts bits to the right X times // 9
	h               = 0b00010010 >> 2;                               // 4
	// Expected result: 00000100 // Does not wrap!                   // Effectively does an integer division by two

	// Left Shift Operator
	unsigned char i = 0b00010010 << 1;
	// Expected result: 00100100 // Shifts bits to the left X times - be careful of overflow

	// Can do compound assignment with most operators
	// b &= 17;

	cout << bitset<8>(i) << endl;

	cin.get();
	return 0;
}