#include <iostream>
using namespace std;

void MyFunction(int i)
{
	cout << "This is an integer." << endl;
}

void MyFunction(float f)
{
	cout << "This is a float." << endl;
}


int main(int argc, char* argv[])
{
	// Typecasting - we have an int and want to convert it into a float.
	int i = 0;
	float f = (float)i;
	// You can also typecast an into to a float like a function call...
	float g = float(i);
	// But this can be dangerous when trying to typecast incompatible formats.

	// The modern C++ way - using a special cast
	// Does compile-time testing to ensure compatibility (works most of the time...)
	float h = static_cast<float>(i);

	// Disambiguating function calls
	// To call the float version with an integer, we can static cast it into a float first...
	MyFunction(static_cast<float>(i));

	// Forcing a type conversion
	int grades[] = {90, 100, 78, 91};
	int count = 4;
	int sum = 0;
	// We can sum the grades easily...
	for (int i = 0; i < count; i++)
	{
		sum += grades[i];
	}
	// ...but we need to use a float to find the average. This won't give us the right average because it's dividing an int over an int
	// We need to force a type conversion on one of the ints to get it to work correctly
	float average = (float)sum / count;



	cin.get();
	return 0;
}