#include <iostream>
#include "ArrayHolder.h"
using namespace std;

int main(int argc, char* argv[])
{
	// These objects are allocated on the stack, but each of them in turn allocates memory on the heap...
	ArrayHolder a;
	ArrayHolder b;

	// C++ generates an assignment operator for classes - a shallow copy.
	b = a;
	// Since these point at the same memory now, at the end of the function when they are destroyed, the memory gets freed twice. Oops!
	
	// Rule of three - when implementing one of three special functions, probably implement all three of them:
	// 1. Destructor function (indicates that there is cleaning up we have to do)
	// 2. Copy constructor
	// 3. Copy assignment operator
	// With all of them, we can make sure the memory used by the object is not messed around with.

	cin.get();
	return 0;
}