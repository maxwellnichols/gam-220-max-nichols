#pragma once

// Example of RAII - Resource aqcusition is initialization
// Acquires memory and releases it when needed
class ArrayHolder
{
public:
	ArrayHolder() // Wait, we also need a destructor since we're allocating heap memory here
	{
		array = new int[50];
		size = 50;
	}

	// const means that ArrayHolder will never change - we are just reading values from it. It will enforce it too - we cannot modify ArrayHolder here
	// We are using a const reference here to avoid making extra copies every time we use this function
	ArrayHolder(const ArrayHolder& other) // Copy constructor (2/3)
	{
		// We can just grab the size from the other object
		size = other.size;
		// Array needs to be different...
		array = new int[size];

		for (int i = 0; i < size; i++)
		{
			array[i] = other.array[i];
		}
	}

	~ArrayHolder() // Here it is! (1/3) But it can still cause problems alone. We need the other two of the three special functions
	{
		delete[] array;
	}

	ArrayHolder& operator=(const ArrayHolder& other) // Copy assignment operator (3/3)
	{
		size = other.size;
		// We need to delete the existing array in this one
		delete[] array;
		array = new int[size];

		for (int i = 0; i < size; i++)
		{
			array[i] = other.array[i];
		}

		// Dereference this object and return it
		return *this;
	}

	int& operator[](int index) // Treat it like an array using an operator overload
	{
		return array[index];
	}

	// const here declares that this function should never be able to change any variables in this class
	// This allows us to use this function with a const object! IE (const ArrayHolder& other) -> other.Size();
	int Size() const
	{
		return size;
	}

private:
	int* array;
	int size;
};