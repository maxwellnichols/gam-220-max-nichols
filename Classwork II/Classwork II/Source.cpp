#include <iostream>
using namespace std;

// This function can only take in floats. If passing in an int, it has to convert it
// We could overload this function to make an int version, but that can quickly get out of hand
float GetMax(float a, float b)
{
	// Return one or the other
	if (a > b)
	{
		return a;
	}
	else
	{
		return b;
	}
}

// With templates, we can specify a generic type
template<typename T>
// Recreate this function with a template instead - tells the compiler how to create functions
T GetMaxTemplate(T a, T b)
{
	// Return one or the other
	if (a > b)
	{
		return a;
	}
	else
	{
		return b;
	}
}

int main(int argc, char* argv[])
{
	// Init some vars
	int a = 4;
	int b = 6;

	// But wait, this function uses floats... it will take some computation time to convert ints to floats and vice versa
	// int c = GetMax(a, b);
	// Instead, we will use the template function - which tells the compiler to generate a function that uses, in this case, integer inputs
	int c = GetMaxTemplate(a, b);

	cout << "The greater value is: " << c << endl;


	cin.get();
	return 0;
}