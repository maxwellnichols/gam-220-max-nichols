#include <iostream>
#include <set>
#include <map>
using namespace std;

int main(int argc, char* argv[])
{
	// Set is similar to a mathematical set - useful for keeping track of unique entries
	set<int> mySet;

	// Sets are not in any particular order
	mySet.insert(5);
	mySet.insert(2);
	mySet.insert(8);
	mySet.insert(1); // 1, 2, 5, 8 - The numbers in a set will sort automatically. If there are duplicates, only one will be listed

	// We can walk through it with an iterator
	for (set<int>::iterator e = mySet.begin(); e != mySet.end(); e++)
	{
		cout << *e << " ";
	}
	cout << endl;

	// Check to see if it contains a value - returns an iterator that points at the value, if it finds one. If not it will point to mySet.end()
	set<int>::iterator f = mySet.find(8);
	// Check if valid
	if (f != mySet.end())
	{
		cout << "Found 8!" << endl;
	}
	else
	{
		cout << "Did not find 8." << endl;
	}
	// Try a number that isn't in there
	f = mySet.find(167);

	if (f != mySet.end())
	{
		cout << "Found 167!" << endl;
	}
	else
	{
		cout << "Did not find 167." << endl;
	}

	// Map - associative array. Basically a dictionary in C#
	map<string, int> myMap;

	myMap.insert(make_pair("Hello", 5));
	myMap.insert(make_pair("Jon", 4));
	myMap.insert(make_pair("This is a string", 19));

	// Iterate through - let the compiler figure it out
	for (auto e = myMap.begin(); e != myMap.end(); e++)
	{
		// Dereferencing e gives us a pair
		cout << e->first << ", " << e->second << endl;
	}

	cin.get();
	return 0;
}