#include <iostream>
using namespace std;

class ClampedInt // an integer, but with a range restriction
{
public:
	int value;
	int minValue;
	int maxValue;

	// Default constructor
	ClampedInt()
	{
		value = 0;
		minValue = -10;
		maxValue = 10;
	}
	ClampedInt(int val, int minVal, int maxVal)
	{
		value = val;
		minValue = minVal;
		maxValue = maxVal;
	}

	// If we add an integer to a clamped integer, we want it to return a clamped integer
	// + is a binary operator - takes two operands. The first operator is implicit (this class)
	ClampedInt operator+(int b)
	{
		// Set up our clamped int
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;
		// Do the addition
		i.value = i.value + b;

		// Check for clamping
		if (i.value < i.minValue)
		{
			i.value = i.minValue;
		}
		else if (i.value > i.maxValue)
		{
			i.value = i.maxValue;
		}

		return i;
	}

	// - is a unary operator - takes one operand. Not subtraction - but negation.
	ClampedInt operator-()
	{
		// Set up our clamped int
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;
		// Do the negation
		i.value = -i.value;

		// Check for clamping
		if (i.value < i.minValue)
		{
			i.value = i.minValue;
		}
		else if (i.value > i.maxValue)
		{
			i.value = i.maxValue;
		}

		return i;
	}

	// - can also work to set up subtraction if done right
	ClampedInt operator-(int b)
	{
		// Set up our clamped int
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;
		// Do the subtraction
		i.value = i.value - b;

		// Check for clamping
		if (i.value < i.minValue)
		{
			i.value = i.minValue;
		}
		else if (i.value > i.maxValue)
		{
			i.value = i.maxValue;
		}

		return i;
	}
};

int main(int argc, char* argv[])
{
	ClampedInt i(4, -14, 12);

	i = i + 16;
	cout << i.value << endl; // Should get clamped at 12

	i = -i;
	cout << i.value << endl; // Should give us -12



	cin.get();
	return 0;
}