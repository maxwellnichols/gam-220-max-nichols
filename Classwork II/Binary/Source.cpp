#include <iostream>
#include <bitset> // Allows us to treat binary as a set of bits
using namespace std;

int main(int argc, char* argv[])
{
	// Binary is a base two system - two possible values per digit, 0 and 1
	// C++ doesn't recognize binary strings as anything special - we need to explicitly tell the program that a number is in binary
	int i = 0b100101;             // 37

	// Formatting integers as binary
	cout << 0b100101 << endl;     // This formats the binary as a decimal
	cout << bitset<8>(i) << endl; // Bitset<number of bits> outputs the actual binary number

	// Overflow
	// Maximum value we can store using 8 bits is 255
	// A char is 8 bits... what happens if we overflow it?
	// unsigned means that there is no negative possible for that variable
	unsigned char c = 255;
	c++;
	cout << (int)c << endl; // When we typecast it to show its value as an int, we see that it wraps back down to 0... -> 11111111 + 1 = 00000000
	// This happens to all numeric data types - while you can store REALLY big numbers, you eventually run out of room

	// Underflow can happen too. 00000000 - 1 = 11111111


	cin.get();
	return 0;
}