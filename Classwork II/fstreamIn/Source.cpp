#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// Set up a datastructure for our data
class Record
{
public:
	string name;
	int level = 0;
	int hitpoints = 0;
	int gold = 0;
};

int main(int argc, char* argv[])
{
	// Create an input filestream object
	ifstream fin("someData.txt");

	while (fin.good()) // Keep doing this until the filestream goes bad. If an interpretation fails (including reaching the end of the file), exit the loop
	{
		// We need a record and an input to work with this file
		string input;
		Record record;

		getline(fin, input); // Skip the "Record" label
		getline(fin, input); // Get the name
		record.name = input;

		fin >> record.level; // Extract the next line (the level) directly into this variable, which magically converts a string to an int for us
		fin >> record.hitpoints; // Same for hitpoints...
		fin >> record.gold;  // Same for gold...

		// Ignore as many characters as possible until a newline is spotted
		fin.ignore(numeric_limits<streamsize>::max(), '\n');

		// Check to see if we are reading the record properly
		cout << record.name << " has " << record.gold << " gold." << endl;
	}

	cin.get();
	return 0;
}