#include <iostream>
using namespace std;

void PrintNumber(float num)
{
	cout << "This is a float:  " << num << endl;
}
// Overload it... now there is a version of it that handles doubles
// Return type can also vary, but it cannot be the only thing that makes an overload different
// The compiler needs to know which one to call - there will be an error if it's ambiguous
void PrintNumber(double num)
{
	cout << "This is a double: " << num << endl;
}

// Can we make different versions of this function with different amounts of numbers to be added?
// a, b, and c are mandatory; d and everything past are optional and if not specified will be 0 - this is also overloading
int AddNumbers(int a, int b, int c, int d = 0, int e = 0)
{
	return a + b + c + d + e;
}

int main(int argc, char* argv[])
{
	// We can call it like this...
	PrintNumber(3.0f);
	// Or we can call it with other values
	// 3.0 is a double, compiler can also convert this (there may be data loss, doubles store more information than floats)
	PrintNumber(3.0);
	// 3 is an integer, and the compiler knows how to convert an int to a float or a double - but it doesn't know which overload to use - ambiguous call!
	// PrintNumber(3);

	cin.get();
	return 0;
}