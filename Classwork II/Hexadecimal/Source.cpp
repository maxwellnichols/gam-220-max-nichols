#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// Hexadecimal is base 16 -
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F
	// In hex, "10" = 16

	cout << "Here is a decimal value: " << 16 << endl;
	cout << "This is a hex literal: " << 0x10 << endl;

	// Hex memory addresses
	int* myInt = new int();
	// When we print a memory address, it is always printed in hexadecimal. It is a little more compact than decimal which allows more info to be stored
	cout << "Memory address: " << myInt << endl;
	delete myInt;

	// Hex colors
	unsigned int color = 0xFFA6BB00; // The maximum that two digits together can be is 255 - in color, that's maximum red/green/blue/opacity
									 // An integer is 4 bytes so this fits perfectly

	// Formatting output for hex
	// Use a stream manipulator to treat ints as hexadecimal
	// Both are actually 32, but they will both display in hex as "20"
	cout << std::hex << 32 << ", " << 0x20 << endl;

	// Revert the stream back to decimal once you're done with your hex output
	cout << 15 << endl; // Outputs F
	cout << std::dec;   // Reverts the stream
	cout << 15 << endl; // Outputs 15

	// Powers of two in hex
	// In binary:
	// 0b0001 // 2^0
	// 0b0010 // 2^1
	// 0b0100 // 2^2 ...

	// In hex there is also a pattern:
	// 0x0001 // 2^0
	// 0x0002 // 2^1
	// 0x0004 // 2^2
	// 0x0008 // 2^3
	// 0x0010 // 2^4
	// 0x0020 // 2^5
	// 0x0040 // 2^6
	// 0x0080 // 2^7
	// 0x0100 // 2^8 ...

	// Hex editors can be used to view executables and debug them (or hack them)

	cin.get();
	return 0;
}