#include <iostream>
#include <list>
using namespace std;

int main(int argc, char* argv[])
{
	// Set up a list
	list<int> myList;

	myList.push_back(37);
	myList.push_front(10);
	myList.push_back(99);
	myList.push_back(0);   // 10, 37, 99, 0 - there is a pointer that connects these in a series - you can't jump around them

	// We can't use square brackets in lists!
	// But we can still use iterator pattern
	for (list<int>::iterator e = myList.begin(); e != myList.end(); e++)
	{
		// We need to dereference the iterator
		cout << *e << " ";
	}
	cout << endl;
	// We need to be careful when iterating over a container AND modifying it

	cin.get();
	return 0;
}