#pragma once
#include "Animal.h"
// For the Speak function
#include <iostream>
using namespace std;

class Cow : public Animal
{
public:
	// Define the speak function for a cow - override makes sure we override the virtual function
	virtual void Speak() override
	{
		cout << "Moo!" << endl;
	}
	// We need to include the GetName function
	virtual string GetName() override
	{
		return "Cow";
	}
};