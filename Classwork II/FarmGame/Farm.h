#pragma once
#include <iostream>
#include "GridObject.h"
using namespace std;

// Define everything a farm can do and store
class Farm
{
public:

	// Will reference a 2D GridObject - 4 x 4
	GridObject* grid[4][4];

	// We actually need the GridObject elements to all be null by default, so we need a constructor
	Farm()
	{
		/*
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				grid[x][y] = nullptr;
			}
		}
		*/

		// Set all of these pointers to 0 - that is the value of nullptr
		// Sets 64 bytes of memory to 0
		memset(grid, 0, 16 * sizeof(GridObject*));
	}

	void Draw()
	{
		cout << "     1     2     3     4" << endl;

		// Go over every spot in the grid
		for (int x = 0; x < 4; x++)
		{
			// Row seperator
			cout << "  -------------------------" << endl;
			cout << (char)('A' + x) << " "; // A -> B -> C -> D

			for (int y = 0; y < 4; y++)
			{
				// Column seperator
				cout << "|";

				// If there is nothing in this space, print an empty block
				if (grid[x][y] == nullptr)
				{
					cout << "     ";
				}
				// If not null, get the object's name
				else
				{
					// But this might be more or less than 5 characters and mess up our grid...
					// substr lets us pick out a piece of it (first character, last character)
					// We also need to know how much filler space we need
					string s = grid[x][y]->GetName().substr(0, 5);
					cout << s;
					// For every character less than 5, print a space
					for (int i = s.length(); i < 5; i++)
					{
						cout << " ";
					}
				}
			}

			// Ending column
			cout << "|" << endl;
		}

		// Ending bottom lines
		cout << "  -------------------------" << endl;
	}

	// Whether the grid has available spaces for new animals
	bool HasSpaceLeft()
	{
		// Go over every spot in the grid
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				// Check if the pointer to this location is null - if yes, we have space
				if (grid[x][y] == nullptr)
				{
					return true;
				}
			}
		}
		// If we found nothing that was null, the grid is full...
		return false;
	}

	// Add an item to the grid - this will work with all animals because they all inherit from GridObject
	void Add(GridObject* obj)
	{
		// Go over every spot in the grid
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				// Check if the pointer to this location is null - if yes, this is where we should add the new item
				if (grid[x][y] == nullptr)
				{
					// This spot will get this object
					grid[x][y] = obj;

					return;
				}
			}
		}

		// If we don't find a spot, do nothing. We will hopefully handle cases like this outside this function
	}

	void PassTurn()
	{
		// Go over every spot in the grid
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				// Check if the pointer to this location is NOT null - if there is an object there, make it do something
				if (grid[x][y] == nullptr)
				{
					grid[x][y]->PassTurn();
				}
			}
		}
	}
};