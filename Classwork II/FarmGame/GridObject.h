#pragma once
#include <string>
using namespace std;

// Set up our grid object
class GridObject
{
public:
	// We cannot instantiate a GridObject, only one of its children. We need to know its name
	virtual string GetName() = 0;
	// This function should have default functionality. Other stuff will still overwrite it
	virtual void PassTurn()
	{
		// THERE'S NOTHING THERE
	}
};