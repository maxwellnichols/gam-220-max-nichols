#pragma once
#include "GridObject.h"

// Inherit from GridObject
class Animal : public GridObject
{
public:
	// What does the animal say?
	// virtual allows this function to be overwritten by the derived classes
	// But we don't know what an "animal" would say, so we use "= 0;" to make it a PURE virtual function that HAS to be overwritten
	virtual void Speak() = 0;
};