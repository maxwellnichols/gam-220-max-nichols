#pragma once
#include <iostream>
#include <string>
#include "Farm.h"
#include "Cow.h"
#include "Chicken.h"
#include "Pig.h"
using namespace std;

// Will have access to all objects and how they interact
class Game
{
public:

	// Store the farm object
	Farm farm;
	// Money - this could be stored in a player object with other stats, but we can save it here
	int money;
	// Keep track of the current time in hours
	int currentTime;

	// We need to construct the object to init money
	Game()
	{
		money = 2000;
		currentTime = 8;
	}

	void Run()
	{
		// whether to quit the game
		bool done = false;

		while (!done)
		{
			// Clear screen
			Clear();

			// Draw farm
			farm.Draw();

			// Ask for user input
			cout << "What would you like to do?" << endl;
			cout << "1) Buy an Animal" << endl << "2) Feed an animal" << endl << "3) Visit an animal" << endl << "4) Pass a turn" << endl << "5) Quit" << endl;
			string input = Prompt();

			// Respond based on user input
			if (input == "1")
			{
				DoBuy();
			}
			else if (input == "2")
			{
				DoFeed();
			}
			else if (input == "3")
			{
				DoVisit();
			}
			else if (input == "4")
			{
				DoPassTurn();
			}
			else if (input == "5")
			{
				done = true;
			}
			else
			{
				cout << "That is not a valid option." << endl;
				cin.get();
			}
		}
	}

	void Clear()
	{
		// We can clear the screen with a special character code
		// This is a really bad way to do this and will only work on Windows
		// It's really bad because someone could replace "cls" with any program - big problem...
		system("cls");
	}

	string Prompt()
	{
		// Get user input and return it
		string result;

		cout << "> ";
		getline(cin, result);

		return result;
	}

	void DoPassTurn()
	{
		cout << "8 hours pass..." << endl;
		currentTime += 8;
		// Wrap time
		if (currentTime >= 24)
		{
			currentTime -= 24;
		}

		// Give farm objects a chance to do something
		farm.PassTurn();

		cin.get();
	}

	// We will use this function to buy various objects, using a template to deal with the different classes it might need to make.
	template<typename T>
	void TryBuyObject(string name, int cost)
	{
		// Check if the player has enough money
		if (money >= cost)
		{
			// Does the player have enough space?
			if (farm.HasSpaceLeft())
			{
				// Complete transaction
				money -= cost;
				cout << "You bought a " << name << "!" << endl;
				cout << "You have $" << money << " left." << endl;

				// Create the cow via heap allocation - we need to keep track of it outside this function
				T* obj = new T();
				// Add it to the grid
				farm.Add(obj);
			}
			// Not enough space
			else
			{
				cout << "You have no more space for a new " << name << "." << endl;
			}
		}
		// Not enough money
		else
		{
			cout << "You do not have enough money for a new " << name << "." << endl;
		}
		// Wait for enter
		cin.get();
	}

	void DoBuy()
	{
		// Loop forever, until the player chooses to return
		while (true)
		{
			Clear();
			cout << "Buying animals..." << endl << endl;
			cout << "You have $" << money << ". What would you like to buy?" << endl;
			cout << "1) Cow ($1000)" << endl << "2) Chicken ($100)" << endl << "3) Pig ($200)" << endl << "4) Return" << endl;

			// Get user input
			string input = Prompt();

			// Cow
			if (input == "1")
			{
				// Use the template function we made to handle this
				TryBuyObject<Cow>("Cow", 1000);
			}
			// Chicken
			else if (input == "2")
			{
				TryBuyObject<Chicken>("Chicken", 100);
			}
			// Pig
			else if (input == "3")
			{
				TryBuyObject<Pig>("Pig", 200);
			}
			else if (input == "4")
			{
				// Leave the function
				return;
			}
			else
			{
				cout << "That is not a valid option." << endl;
				cin.get();
			}
		}
	}

	void DoFeed()
	{
		// TODO
	}

	void DoVisit()
	{
		Clear();
		farm.Draw();
		// Ask user to choose an animal
		cout << "Which animal? (ex. A2)" << endl;
		string input = Prompt();

		// Check for input validity
		if (input.size() != 2)
		{
			// Invalid, return to menu...
			return;
		}
		// We know for sure that there are only two letters in the input now, so we should keep track of them
		char first = input[0];
		char second = input[1];

		if (first < 'A' || first > 'D')
		{
			return;
		}
		if (second < '1' || second > '4')
		{
			return;
		}

		// Now we know that this is a valid combination within the grid's range
		// Find an index in the grid by subtracting the input from the first possible element (so A = 0, B = 1, etc...)
		int xIndex = first - 'A';
		int yIndex = second - '1';
		// Get the animal
		GridObject* obj = farm.grid[yIndex][xIndex];

		// If null... there's nothing there
		if (obj == nullptr)
		{
			cout << "You are standing in an empty field. This is not fun." << endl;
			cin.get();
			return;
		}

		Clear();
		cout << "You see a " << obj->GetName() << endl;

		// Try to convert the grid object to an animal - does runtime type identification. If successful, gives a pointer. If unsuccessful, gives null.
		Animal* animal = dynamic_cast<Animal*>(obj);
		// Not an animal.
		if (animal == nullptr)
		{
			cin.get();
			return;
		}
		// It is definitely an animal.
		cout << "The animal says...";
		animal->Speak();
		cout << endl;

		// Is the animal a chicken?
		Chicken* chicken = dynamic_cast<Chicken*>(animal);
		if (chicken == nullptr)
		{
			cin.get();
			return;
		}

		if (chicken->GetNumEggs() == 0)
		{
			cout << "The chicken has no eggs." << endl;
		}
		else
		{
			cout << "The chicken has " << chicken->GetNumEggs() << " eggs." << endl;
			cout << "You collect the eggs, worth $" << 3 * chicken->GetNumEggs() << "." << endl;
			money += 3 * chicken->CollectEggs();
		}

		cin.get();
	}
};