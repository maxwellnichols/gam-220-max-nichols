#pragma once
#include "Animal.h"
// For the Speak function
#include <iostream>
using namespace std;

class Chicken : public Animal
{
private:
	int timeUntilEgg;
	int numEggs;
public:
	Chicken()
	{
		timeUntilEgg = 3;
		numEggs = 0;
	}

	// Define the speak function for a chicken
	virtual void Speak() override
	{
		cout << "Cluck!" << endl;
	}

	virtual string GetName() override
	{
		return "Chicken";
	}
	// Chickens produce one egg every day
	virtual void PassTurn() override
	{
		// One turn closer to an egg
		timeUntilEgg--;

		// Check if an egg is ready
		if (timeUntilEgg <= 0)
		{
			timeUntilEgg = 3;
			numEggs++;
		}
	}
	// const because asking how many eggs it has should not change the chicken
	int GetNumEggs() const
	{
		return numEggs;
	}

	int CollectEggs()
	{
		int n = numEggs;
		numEggs = 0;

		return n;
	}
};