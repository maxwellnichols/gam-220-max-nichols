#include <iostream>
#include "Game.h"
using namespace std;

int main(int argc, char* argv[])
{
	// Set up the game
	Game game;
	// Run the game
	game.Run();

	return 0;
}