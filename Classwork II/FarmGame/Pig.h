#pragma once
#include "Animal.h"
// For the Speak function
#include <iostream>
using namespace std;

class Pig : public Animal
{
public:
	// Define the speak function for a pig
	virtual void Speak() override
	{
		cout << "Oink!" << endl;
	}

	virtual string GetName() override
	{
		return "Pig";
	}
};