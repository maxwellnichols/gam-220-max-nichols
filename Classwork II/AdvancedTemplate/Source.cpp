#include <iostream>
#include <string>
using namespace std;

// Specify multiple type parameters...
// Can lead us easily into metaprogramming - writing code that generates code
template<typename U, typename V>
class Pair
{
public:
	U first;
	V second;
};

int main(int argc, char* argv[])
{
	Pair<int, int> ii;
	Pair<string, int> si;

	// Using multiple parameters we can do stuff like this
	si.first = "Hello.";
	si.second = 4;

	cin.get();
	return 0;
}