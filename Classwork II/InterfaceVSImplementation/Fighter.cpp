#include <iostream> // ...from implementation (this file).
#include "Fighter.h"
using namespace std;

Fighter::Fighter()
{
	hitpoints = 10;
	attackPower = 2;
}

// Because we included the header, the function below knows about the hitpoints variable
void Fighter::TakeDamage(int damage)
{
	cout << "You take damage." << endl;
	hitpoints -= damage;
}