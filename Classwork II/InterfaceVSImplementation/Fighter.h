#pragma once // We like to seperate interface (this file)...

class Fighter
{
public:
	int hitpoints;
	int attackPower;
	
	Fighter(); // Constructor

	void TakeDamage(int damage); // We don't have the definition for the function yet... we are going to say what it does in another file
};