#pragma once

class Goblin
{
public:
	int hitpoints;
	int attackPower;

	Goblin();

	void TakeDamage(int damage);
};