#include <iostream>
#include "Fighter.h"
#include "Goblin.h"
using namespace std;

int main(int argc, char* argv[])
{
	// Fight to the death!!
	Fighter f;
	Goblin g;

	// While neither character has 0 hitpoints, keep fighting
	while (f.hitpoints > 0 && g.hitpoints > 0)
	{
		g.TakeDamage(f.attackPower);
		f.TakeDamage(g.attackPower);
	}

	if (f.hitpoints > 0)
	{
		cout << "The fighter has won. He has " << f.hitpoints << " hit points remaining." << endl;
	}
	if (g.hitpoints > 0)
	{
		cout << "The goblin has won. He has " << g.hitpoints << " hit points remaining." << endl;
	}

	cin.get();
	return 0;
}