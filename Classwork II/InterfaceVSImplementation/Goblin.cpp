#include <iostream>
#include "Goblin.h"
using namespace std;

Goblin::Goblin()
{
	hitpoints = 6;
	attackPower = 1;
}

void Goblin::TakeDamage(int damage)
{
	cout << "The goblin takes damage." << endl;
	hitpoints -= damage;
}