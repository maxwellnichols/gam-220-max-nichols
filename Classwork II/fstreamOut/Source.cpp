#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void DoStuff()
{
	string input;
	cout << "What is your name?" << endl;
	getline(cin, input);

	// Create an output filestream object
	ofstream fout;
	// Open this file...
	fout.open("myFile.txt");
	// Print to this file, just like cout
	fout << "Name: " << input << endl;

	cout << "Tell me something about yourself." << endl;
	getline(cin, input);
	// File is still open...
	fout << "Data: " << input << endl;

	cout << endl << "Thank you." << endl;

	// Close the file
	fout.close();

	// We can also open a file directly with "ofstream fout("myFile.txt");" and letting the destructor close the file at the end of the function
}

int main(int argc, char* argv[])
{
	DoStuff();

	cin.get();
	return 0;
}